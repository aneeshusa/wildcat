`ifndef RS232_RECEIVE_CHAR_V
`define RS232_RECEIVE_CHAR_V

`include "./src/hdl/prologue.v"


// Takes an incoming RS232 stream on `input_bit`,
// with availability signaled on `input_enable`.
// Decodes RS232 Frames with `DATA_BITS` data bits
// and `STOP_BITS` stop bits and checks validity.
// Output is posted on `output_char`,
// with availability signaled on `output_enable`.
// TODO: Add support for parity bits.
// TODO: test busy
module rs232_receive_char #(
    parameter DATA_BITS = 8,
    parameter STOP_BITS = 1 // Must be 1 or 2
) (
    input wire clock, reset,
    input wire input_enable, input_bit,
    // Pulses high for one cycle when a new command is available
    // This will happen one cycle after the last data bit is
    // available on `input_bit` (when `input_enable` is asserted)
    output reg output_enable = 1'b0,
    // Bits will be stored msb-to-lsb (wire order is lsb-to-msb).
    output reg [DATA_BITS - 1:0] output_char = {DATA_BITS{1'b0}},
    output reg busy = 1'b0
);

    localparam STATE_READ_START_BIT = 2'b00;
    localparam STATE_READ_DATA_BIT =  2'b01;
    localparam STATE_READ_STOP_BIT =  2'b11;
    localparam DEFAULT_STATE = STATE_READ_START_BIT;
    reg [1:0] state = DEFAULT_STATE;

    // Counter of how many data bits we have seen so far
    localparam DATA_BITS_WIDTH = `CLOG2(DATA_BITS - 1);
    reg [DATA_BITS_WIDTH - 1:0] data_bits = {DATA_BITS_WIDTH{1'b0}};

    // Buffer the in-progress character as it is received
    reg [DATA_BITS - 1:0] data_buf = {DATA_BITS{1'b0}};

    // Counter of how many stop bits we have seen so far
    localparam STOP_BITS_WIDTH = `CLOG2(STOP_BITS - 1);
    reg [STOP_BITS_WIDTH - 1:0] stop_bits = {STOP_BITS_WIDTH{1'b0}};


    `define rs232_receive_char__restart() begin \
        state <= DEFAULT_STATE; \
        data_bits <= {DATA_BITS_WIDTH{1'b0}}; \
        data_buf <= {DATA_BITS{1'b0}}; \
        stop_bits <= {STOP_BITS_WIDTH{1'b0}}; \
    end


    `define rs232_receive_char__reset() begin \
        `rs232_receive_char__restart \
        output_enable <= 1'b0; \
        output_char <= {DATA_BITS{1'b0}}; \
        busy <= 1'b0; \
    end


    always @(posedge clock) begin
        if (reset) begin
            `rs232_receive_char__reset
        end else begin
            if (input_enable == 1'b0) begin
                output_enable <= 1'b0;
                busy <= 1'b0;
            end else if (input_enable == 1'b1) begin
                case (state)
                    STATE_READ_START_BIT: begin
                        output_enable <= 1'b0;
                        if (input_bit == 1'b0) begin
                            state <= STATE_READ_DATA_BIT;
                            busy <= 1'b1;
                        end else begin
                           `rs232_receive_char__restart
                        end
                    end
                    STATE_READ_DATA_BIT: begin
                        data_buf[data_bits] <= input_bit;
                        if (data_bits < DATA_BITS - 1) begin
                            data_bits <= data_bits + 1;
                        end else begin
                            state <= STATE_READ_STOP_BIT;
                        end
                    end
                    STATE_READ_STOP_BIT: begin
                        if (input_bit != 1'b1) begin
                            busy <= 1'b0;
                            `rs232_receive_char__restart
                        end else begin
                            if (stop_bits < STOP_BITS - 1) begin
                                stop_bits <= stop_bits + 1;
                            end else begin
                                output_enable <= 1'b1;
                                output_char <= data_buf;
                                busy <= 1'b0;
                                `rs232_receive_char__restart
                            end
                        end
                    end
                    default: begin
                        `rs232_receive_char__reset
                    end
                endcase
            end else begin
                `rs232_receive_char__reset
            end
        end
    end

endmodule

`endif // RS232_RECEIVE_CHAR_V
