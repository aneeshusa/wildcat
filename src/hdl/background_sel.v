`ifndef BACKGROUND_SEL_V
`define BACKGROUND_SEL_V

`include "./src/hdl/prologue.v"


module background_sel(
    input wire clock,
    input wire reset,
    input wire char_pixel,
    input wire authorized_enable,
    input wire unauthorized_enable,
    output reg [3:0] r_val,
    output reg [3:0] g_val,
    output reg [3:0] b_val
);

    always @(posedge clock) begin
        if (authorized_enable) begin
            if (!char_pixel) begin
                r_val <= {4{char_pixel}};
                g_val <= {4{char_pixel}};
                b_val <= {4{char_pixel}};
            end else begin
                r_val <= 4'b0000;
                g_val <= 4'b1111;
                b_val <= 4'b0000;
            end
        end else if (unauthorized_enable) begin
            if (!char_pixel) begin
                r_val <= {4{char_pixel}};
                g_val <= {4{char_pixel}};
                b_val <= {4{char_pixel}};
            end else begin
                r_val <= 4'b1111;
                g_val <= 4'b0000;
                b_val <= 4'b0000;
            end
        end else begin
            r_val <= {4{char_pixel}};
            g_val <= {4{char_pixel}};
            b_val <= {4{char_pixel}};
        end
    end
endmodule


`endif // BACKGROUND_SEL_V
