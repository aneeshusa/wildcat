`ifndef SYNCHRONIZE_V
`define SYNCHRONIZE_V

`include "./src/hdl/prologue.v"


// Module to synchronize inputs coming from a different clock domain,
// e.g. for inputs from the outside world.
// No testbench since this module was provided.
module synchronize #(
    // Number of synchronizing registers. Must be >= 2.
    parameter NUM_SYNC = 2
) (
    input wire clock,
    input wire reset,
    input wire raw,
    output wire synchronized
);

    reg synchronized_val = 1'b0;
    assign synchronized = synchronized_val;

    reg [NUM_SYNC - 2:0] sync;

    always @(posedge clock) begin
        if (reset) begin
            {synchronized_val, sync} <= {NUM_SYNC{1'b0}};
        end else begin
            {synchronized_val, sync} <= {sync, raw};
        end
    end

endmodule

`endif // SYNCHRONIZE_V
