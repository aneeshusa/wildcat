
`ifndef SERIAL_OUTPUT_SEL_V
`define SERIAL_OUTPUT_SEL_V

`include "./src/hdl/prologue.v"

module serial_output_sel(
    input wire clock,
    input wire serial_echo_enable,
    input wire [7:0] serial_echo_char,
    input wire username_serial_prompt_enable,
    input wire [7:0] username_serial_prompt_char,
    input wire password_serial_prompt_enable,
    input wire [7:0] password_serial_prompt_char,
    input wire password_enable,
    input wire id_tap_serial_prompt_enable,
    input wire [7:0] id_tap_serial_prompt_char,
    input wire id_tap_enable,
    input wire id_num_serial_prompt_enable,
    input wire [7:0] id_num_serial_prompt_char,
    input wire id_number_enable,
    input wire bel_enable,
    output reg serial_enable,
    output reg [7:0]serial_send_char
);

    localparam CHAR_ENTER = 8'h0D;
    localparam CHAR_ASTERISK = 8'h2A;
    localparam CHAR_BEL = 8'h07;

    always @(posedge clock) begin
         if (bel_enable) begin
             serial_enable <= 1'b1;
             serial_send_char <= CHAR_BEL;
         end else if (username_serial_prompt_enable) begin
             serial_enable <= 1'b1;
             serial_send_char <= username_serial_prompt_char;
         end else if (password_serial_prompt_enable) begin
             serial_enable <= 1'b1;
             serial_send_char <= password_serial_prompt_char;
         end else if (id_tap_serial_prompt_enable) begin
             serial_enable <= 1'b1;
             serial_send_char <= id_tap_serial_prompt_char;
         end else if (id_num_serial_prompt_enable) begin
             serial_enable <= 1'b1;
             serial_send_char <= id_num_serial_prompt_char;
         end else if (serial_echo_enable && id_tap_enable) begin
             serial_enable <= 1'b0;
         end else if (serial_echo_enable && id_number_enable) begin
             serial_enable <= 1'b0;
         end else if (serial_echo_enable && password_enable) begin
             serial_enable <= 1'b1;
             serial_send_char <= CHAR_ASTERISK;
         end else if (serial_echo_enable) begin
             serial_enable <= 1'b1;
             serial_send_char <= serial_echo_char;
         end else begin
             serial_enable <= 1'b0;
         end
     end
 endmodule


`endif // SERIAL_OUTPUT_SEL_V
