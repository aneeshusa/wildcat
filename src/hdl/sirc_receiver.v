`ifndef SIRC_RECEIVER_V
`define SIRC_RECEIVER_V

`include "./src/hdl/prologue.v"

`include "./src/hdl/decode_sirc_command.v"
`include "./src/hdl/divider.v"
`include "./src/hdl/downsample.v"
`include "./src/hdl/sample.v"


// IR SIRC Receiver module.
// Takes a (synchronized) SIRC input signal,
// and outputs BLAH TODO.
module sirc_receiver #(
    parameter OVERCARD_PERIOD = 1000,
    parameter OVERSAMPLE_RATIO = 10,
    parameter COMMAND_WIDTH = 7,
    parameter ADDRESS_WIDTH = 5
) (
    input wire clock, reset,
    input wire sirc_input,
    output wire output_enable,
    output wire [COMMAND_WIDTH + ADDRESS_WIDTH - 1:0] output_command
);

    localparam SIRC_WIDTH = COMMAND_WIDTH + ADDRESS_WIDTH;

    wire oversirc_enable;
    divider #(
        .PERIOD(OVERCARD_PERIOD) // 75us TODO
    ) oversirc_gen(
        .clock(clock), .reset(reset),
        .enable(oversirc_enable)
    );

    wire sirc_in_overenable, sirc_in_oversample;
    sample sirc_in_sample(
        .clock(clock), .reset(reset),
        .sample_enable(oversirc_enable),
        .data_in(sirc_input),
        .output_enable(sirc_in_overenable),
        .data_out(sirc_in_oversample)
    );

    wire receiver_enable, receiver_data;
    downsample #(
        .DOWNSAMPLE_RATIO(OVERSAMPLE_RATIO)
    ) downsample_receiver_input(
        .clock(clock), .reset(reset),
        .input_enable(sirc_in_overenable),
        .data_in(sirc_in_oversample),
        .output_enable(receiver_enable),
        .data_out(receiver_data)
    );

    decode_sirc_command #(
        .SIRC_WIDTH(SIRC_WIDTH),
        .START_DURATION(4)
    ) decode_receiver_data(
        .clock(clock), .reset(reset),
        .input_enable(receiver_enable),
        .input_bit(receiver_data),
        .output_enable(output_enable),
        .output_command(output_command)
    );

endmodule

`endif // SIRC_RECEIVER_V
