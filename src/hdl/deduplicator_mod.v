`ifndef DEDUPLICATOR_V
`define DEDUPLICATOR_V

`include "./src/hdl/prologue.v"
`include "./src/hdl/timer.v"
module sirc_deduplicator_2(
    input wire clock,
    input wire reset,
    input wire sirc_enable,
    input wire [3:0] new_sirc_command,
    output reg sirc_write_enable = 1'b0,
    output reg [3:0] sirc_write = 4'b1111,
    output reg [1:0] debug
);

    wire expired;
    reg new_number = 1'b0;

    timer timer_mod(
        .clock(clock),
        .reset(reset),
        .start(sirc_write_enable),
        .timer_length(1),
        .expired(expired)
    );


    always @(posedge clock) begin
        debug <= {2{new_number}};
        if (reset) begin
            sirc_write_enable <= 1'b1;
            sirc_write <= 4'b1111;
        end else begin
            if (sirc_enable && new_number) begin
                sirc_write <= new_sirc_command;
                new_number <= 1'b0;
                sirc_write_enable <= 1'b1;
            end else if (expired) begin
                new_number <= 1'b1;
                sirc_write_enable <= 1'b0;
            end else begin
                sirc_write_enable <= 1'b0;
            end
        end
      end
endmodule

`endif // DEDUPLICATOR_V
