`ifndef RS232_SEND_CHAR_V
`define RS232_SEND_CHAR_V

`include "./src/hdl/prologue.v"


// FSM which sends values via RS-232, one character at a time.
// Parameterized by number of data bits (char width) and number of stop bits.
// TODO: Add support for parity bits.
// The FSM waits for a request on `start`, then sends the data in `char`
// in an RS-232 frame, asserting `done` for one cycle when
// there are no more bits to send and thus
// the FSM will be ready to accept a new request on the next cycle.
module rs232_send_char #(
    parameter DATA_BITS = 8,
    parameter STOP_BITS = 1 // Must be 1 or 2
) (
    input wire clock, reset,
    input wire start,
    input wire [DATA_BITS - 1:0] char,
    input wire baud_enable,
    output wire transmit,
    output reg done = 1'b0
);

    // NOTE: In RS-232, the wire default is a logical 1.
    // (The physical wire value is low for a logical 1 for RS-232,
    // but that can be ignored as the FTDI chip on the Nexys4
    // is instead conveying the RS-232 data over USB.)
    localparam VALUE_IDLE = 1'b1;
    localparam VALUE_START = 1'b0;
    localparam VALUE_STOP = 1'b1;
    reg transmit_val = VALUE_IDLE;
    assign transmit = transmit_val;

    // Don't have any values with combinational dependencies on `state`,
    // but use a Gray code anways
    localparam STATE_IDLE = 2'b00;
    localparam STATE_SEND_START = 2'b01;
    localparam STATE_SEND_DATA = 2'b11;
    localparam STATE_SEND_STOP = 2'b10;

    localparam DEFAULT_STATE = STATE_IDLE;
    reg [1:0] state = DEFAULT_STATE;

    localparam DATA_WIDTH = `CLOG2(DATA_BITS - 1);
    reg [DATA_WIDTH - 1:0] current_data_bit = {DATA_WIDTH{1'b0}};

    localparam STOP_WIDTH = `CLOG2(STOP_BITS - 1);
    reg [STOP_WIDTH - 1:0] current_stop_bit = {STOP_WIDTH{1'b0}};


    `define rs232_send_char__restart() begin \
        state <= DEFAULT_STATE; \
        current_data_bit <= {DATA_WIDTH{1'b0}}; \
        current_stop_bit <= {STOP_WIDTH{1'b0}}; \
    end


    `define rs232_send_char__reset() begin \
        `rs232_send_char__restart \
        transmit_val <= VALUE_IDLE; \
        done <= 1'b0; \
    end


    always @(posedge clock) begin
        if (reset) begin
            `rs232_send_char__reset
        end else begin
            case (state)
                STATE_IDLE: begin
                    done <= 1'b0;
                    transmit_val <= VALUE_IDLE;
                    if (start) begin
                        state <= STATE_SEND_START;
                    end
                end
                STATE_SEND_START: begin
                    if (baud_enable) begin
                        transmit_val <= VALUE_START;
                        state <= STATE_SEND_DATA;
                    end
                end
                STATE_SEND_DATA: begin
                    if (baud_enable) begin
                        transmit_val <= char[current_data_bit];
                        if (current_data_bit < DATA_BITS - 1) begin
                            current_data_bit <= current_data_bit + 1;
                        end else begin
                            state <= STATE_SEND_STOP;
                        end
                    end
                end
                STATE_SEND_STOP: begin
                    if (baud_enable) begin
                        transmit_val <= VALUE_STOP;
                        if (current_stop_bit < STOP_BITS - 1) begin
                            current_stop_bit <= current_stop_bit + 1;
                        end else begin
                            done <= 1'b1;
                            `rs232_send_char__restart
                        end
                    end
                end
                default: begin
                    `rs232_send_char__reset
                end
            endcase
        end
    end

endmodule

`endif // RS232_SEND_CHAR_V
