`ifndef IDENTITY_DATABASE_V
`define IDENTITY_DATABASE_V

`include "./src/hdl/prologue.v"
`include "./src/hdl/md5.v"


module identity_database #(
    parameter NUM_CHARS = 8,
    parameter ID_NUM_CHARS = 9
) (
    input wire clock, reset,
    input wire start,
    input wire [(8 * NUM_CHARS) - 1:0] username_input,
    input wire [(8 * NUM_CHARS) - 1:0] password_input,
    input wire [(8 * ID_NUM_CHARS) - 1:0] id_number_input,
    output reg done = 1'b0,
    output reg authorized = 1'b0
);

    localparam STATE_IDLE = 2'b00;
    localparam STATE_HASHING = 2'b01;
    localparam STATE_SEARCHING = 2'b10;
    reg [1:0] state = STATE_IDLE;

    // Username is NUM_CHARS wide, with each character being 8 bits,
    // the md5 hashed password is 128 bits,
    // and the id number is ID_NUM_CHARS wide, with 8 bit characters.
    localparam ROM_WIDTH = (NUM_CHARS * 8) + 128 + (ID_NUM_CHARS * 8);
    localparam NUM_ENTRIES = 5;

    // BROM has 1 cycle latency from addr req to value return,
    // so need to be able to wait one extra cycle to check last value
    localparam NUM_ENTRIES_WIDTH = `CLOG2(NUM_ENTRIES + 1);
    reg [NUM_ENTRIES_WIDTH - 1:0] addr = {NUM_ENTRIES_WIDTH{1'b0}};

    (*rom_style = "block" *)
    reg [ROM_WIDTH - 1:0] identity_rom[0:NUM_ENTRIES - 1];

    initial begin
        $readmemh("./build/rom/identity_database.rom", identity_rom);
    end

    reg hasher_start = 1'b0;
    wire hasher_done;
    wire [127:0] hash_digest;
    wire [ROM_WIDTH - 1:0] combined_input = {
        username_input, hash_digest, id_number_input
    };
    md5 #(
        .INPUT_WIDTH(NUM_CHARS * 8)
    ) password_hasher(
        .clock(clock), .reset(reset),
        .start(hasher_start),
        .input_data(password_input),
        .output_enable(hasher_done),
        .output_data(hash_digest)
    );


    `define identity_database__restart() begin \
        state <= STATE_IDLE; \
        addr <= {NUM_ENTRIES_WIDTH{1'b0}}; \
        hasher_start <= 1'b0; \
     end


    `define identity_database__reset() begin \
        `identity_database__restart \
        done <= 1'b0; \
        authorized <= 1'b0; \
    end


    always @(posedge clock) begin
        if (reset) begin
            `identity_database__reset
        end else begin
            case (state)
                STATE_IDLE: begin
                    done <= 1'b0;
                    if (start) begin
                        hasher_start <= 1'b1;
                        state <= STATE_HASHING;
                    end
                end
                STATE_HASHING: begin
                    hasher_start <= 1'b0;
                    if (hasher_done) begin
                        state <= STATE_SEARCHING;
                        addr <= {NUM_ENTRIES_WIDTH{1'b0}};
                    end
                end
                STATE_SEARCHING: begin
                    if (combined_input == identity_rom[addr]) begin
                        done <= 1'b1;
                        authorized <= 1'b1;
                        `identity_database__restart
                    end else if (addr == NUM_ENTRIES) begin
                        done <= 1'b1;
                        authorized <= 1'b0;
                        `identity_database__restart
                    end
                    addr <= addr + 1;
                end
            endcase
        end
    end
endmodule

`endif // IDENTITY_DATABASE_V

