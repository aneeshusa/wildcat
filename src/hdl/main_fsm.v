`ifndef MAIN_FSM_V
`define MAIN_FSM_V

`include "./src/hdl/prologue.v"

`include "./src/hdl/identity_database.v"
`include "./src/hdl/timer.v"

// Main FSM of the system,
// coordinates the entire 2FA login flow
// and interacts with serial, RFID, and SIRC.
// Exposes some state data for output to XVGA.
module main_fsm #(
    parameter CLOCK_FREQUENCY = 100_000_000,
    parameter NUM_CHARS = 8,
    parameter ID_NUM_CHARS = 9
) (
    input wire clock, reset,
    input wire serial_enable,
    input wire [7:0] serial_char_received,
    input wire sirc_enable,
    input wire [3:0] sirc_command,
    input wire [31:0] card_data,
    // Initialize username_input and password_input with CHAR_SPACE
    output reg [(8 * NUM_CHARS) - 1:0] username_input = {NUM_CHARS{8'h20}},
    output reg [(8 * NUM_CHARS) - 1:0] password_input = {NUM_CHARS{8'h20}},
    output reg [(8 * NUM_CHARS) - 1:0] dummy_password_input = {NUM_CHARS{8'h20}},
    output reg [(8 * ID_NUM_CHARS) -1:0] id_number_input = {ID_NUM_CHARS{8'h20}},
    output reg mit_login_enable = 1'b0,
    output reg username_enable = 1'b0,
    output reg username_input_enable = 1'b0,
    output reg password_enable = 1'b0,
    output reg id_tap_enable = 1'b0,
    output reg id_tapped_enable = 1'b0,
    output reg id_number_enable = 1'b0,
    output reg authorized_enable = 1'b0,
    output reg unauthorized_enable = 1'b0,
    output reg username_serial_start_prompt = 1'b0,
    output reg password_serial_start_prompt = 1'b0,
    output reg id_tap_serial_start_prompt = 1'b0,
    output reg id_num_serial_start_prompt = 1'b0,
    output reg bel_enable = 1'b0
);

    localparam default_card_data = 32'hCA83897E;

    localparam CHAR_ENTER = 8'h0D;
    localparam CHAR_SPACE = 8'h20;
    localparam CHAR_BACKSPACE = 8'h7F;
    localparam CHAR_ESCAPE = 8'h1B;

    localparam SEARCHING = 2'b00;
    localparam AUTHORIZED = 2'b01;
    localparam UNAUTHORIZED = 2'b10;

    localparam STATE_IDLE = 4'b0000;
    localparam STATE_SEND_USERNAME_PROMPT = 4'b0001;
    localparam STATE_GET_USERNAME_CHAR = 4'b0010;
    localparam STATE_ECHO_USERNAME_CHAR = 4'b0011;
    localparam STATE_SEND_PASSWORD_PROMPT = 4'b0100;
    localparam STATE_GET_PASSWORD_CHAR = 4'b0101;
    localparam STATE_ECHO_PASSWORD_CHAR = 4'b0110;
    localparam STATE_SEND_ID_TAP_PROMPT = 4'b0111;
    localparam STATE_WAIT_ID_TAP = 4'b1000;
    localparam STATE_ID_NUMBER = 4'b1001;
    localparam STATE_WAIT_VALIDITY = 4'b1010;
    localparam STATE_AUTHORIZED = 4'b1011;
    localparam STATE_UNAUTHORIZED = 4'b1100;
    localparam STATE_ID_NUM_CHECK = 4'b1110;
    reg [3:0] state = STATE_ECHO_USERNAME_CHAR;
    reg [3:0] sirc_num = 4'b0000;
    // Position of cursor is after last entered character,
    // so at most is one more than the number of characters
    localparam POS_WIDTH = `CLOG2(NUM_CHARS + 1);
    reg [POS_WIDTH - 1:0] username_pos = {POS_WIDTH{1'b0}};
    reg [POS_WIDTH - 1:0] password_pos = {POS_WIDTH{1'b0}};
    reg [3:0] id_number_pos = {4{1'b0}};

    reg identity_check_start = 1'b0;
    wire identity_check_done, authorized;
    identity_database #(
        .NUM_CHARS(NUM_CHARS),
        .ID_NUM_CHARS(ID_NUM_CHARS)
    ) identity_database_mod(
        .clock(clock), .reset(reset),
        .start(identity_check_start),
        .username_input(username_input),
        .password_input(password_input),
        .id_number_input(id_number_input),
        .done(identity_check_done),
        .authorized(authorized)
    );


    localparam USERNAME_TIMEOUT = 5'd15;
    localparam PASSWORD_TIMEOUT = 5'd15;
    localparam ID_TAP_TIMEOUT = 5'd15;
    localparam ID_NUMBER_TIMEOUT = 5'd25;
    localparam UNAUTHORIZED_TIMEOUT = 5'd7;
    localparam TIMER_LENGTH_WIDTH = 5;
    reg start_timer = 1'b0;
    reg [TIMER_LENGTH_WIDTH - 1:0] timer_length = {TIMER_LENGTH_WIDTH{1'b0}};
    wire timer_expired;
    timer #(
        .CLOCK_FREQUENCY(CLOCK_FREQUENCY),
        .MAX_TIMER_LENGTH(25)
    ) timeout_timer(
        .clock(clock), .reset(reset),
        .start(start_timer),
        .timer_length(timer_length),
        .expired(timer_expired)
    );


    `define main_fsm__reset() begin \
        username_input <= {NUM_CHARS{CHAR_SPACE}}; \
        password_input <= {NUM_CHARS{CHAR_SPACE}}; \
        dummy_password_input <= {NUM_CHARS{CHAR_SPACE}}; \
        id_number_input <= {ID_NUM_CHARS{CHAR_SPACE}}; \
        mit_login_enable <= 1'b0; \
        username_enable <= 1'b0; \
        username_input_enable <= 1'b0; \
        password_enable <= 1'b0; \
        id_tap_enable <= 1'b0; \
        id_tapped_enable <= 1'b0; \
        id_number_enable <= 1'b0; \
        authorized_enable <= 1'b0; \
        unauthorized_enable <= 1'b0; \
        state <= STATE_IDLE; \
        username_pos <= {POS_WIDTH{1'b0}}; \
        password_pos <= {POS_WIDTH{1'b0}}; \
        identity_check_start <= 1'b0; \
        id_number_pos <= {4{1'b0}}; \
        username_serial_start_prompt <= 1'b0; \
        password_serial_start_prompt <= 1'b0; \
        id_tap_serial_start_prompt <= 1'b0; \
        id_num_serial_start_prompt <= 1'b0; \
        bel_enable <= 1'b0; \
        start_timer <= 1'b0; \
        timer_length <= {TIMER_LENGTH_WIDTH{1'b0}}; \
    end


    always @(posedge clock) begin
         if (reset) begin
              `main_fsm__reset
         end else if (
                serial_enable
             && (serial_char_received == CHAR_ESCAPE)
         ) begin
             `main_fsm__reset
         end else begin
             case(state)
                  STATE_IDLE: begin
                      if (serial_enable) begin
                          if (serial_char_received == CHAR_ENTER) begin
                              state <= STATE_SEND_USERNAME_PROMPT;
                          end
                      end
                  end
                  STATE_SEND_USERNAME_PROMPT: begin
                      username_serial_start_prompt <= 1'b1;
                      start_timer <= 1'b1;
                      timer_length <= USERNAME_TIMEOUT;
                      state <= STATE_ECHO_USERNAME_CHAR;
                  end
                  STATE_ECHO_USERNAME_CHAR: begin
                      mit_login_enable <= 1'b1;
                      username_enable <= 1'b1;
                      username_input_enable <= 1'b1;
                      username_serial_start_prompt <= 1'b0;
                      start_timer <= 1'b0;
                      if (timer_expired) begin
                          `main_fsm__reset
                      end else if (serial_enable) begin
                          if (serial_char_received == CHAR_ENTER) begin
                              state <= STATE_SEND_PASSWORD_PROMPT;
                              bel_enable <= 1'b0;
                          end else if (serial_char_received == CHAR_BACKSPACE) begin
                              bel_enable <= 1'b0;
                              if (username_pos > 0) begin
                                  username_input[(((8 - username_pos + 1) * 8) - 1) -: 8] <= CHAR_SPACE;
                                  username_pos <= username_pos - 1;
                              end
                              // TODO: else send BEL
                          end else if (username_pos == NUM_CHARS) begin
                              bel_enable <= 1'b1;
                          end else if (username_pos != NUM_CHARS) begin
                              username_input[(((NUM_CHARS - username_pos) * 8) - 1) -: 8] <= serial_char_received;
                              username_pos <= username_pos + 1;
                              bel_enable <= 1'b0;
                          end
                      end
                  end
                  STATE_SEND_PASSWORD_PROMPT:begin
                      password_serial_start_prompt <= 1'b1;
                      start_timer <= 1'b1;
                      timer_length <= PASSWORD_TIMEOUT;
                      state <= STATE_ECHO_PASSWORD_CHAR;
                  end
                  STATE_ECHO_PASSWORD_CHAR: begin
                      password_enable <= 1'b1;
                      password_serial_start_prompt <= 1'b0;
                      start_timer <= 1'b0;
                      if (timer_expired) begin
                          `main_fsm__reset
                      end else if (serial_enable) begin
                          if (serial_char_received == CHAR_ENTER) begin
                              state <= STATE_SEND_ID_TAP_PROMPT;
                          end else if (serial_char_received == CHAR_BACKSPACE) begin
                              if (password_pos > 0) begin
                                  password_input[(((8 - password_pos + 1) * 8) - 1) -: 8] <= CHAR_SPACE;
                                  dummy_password_input[(((8 - password_pos + 1) * 8) - 1) -: 8] <= CHAR_SPACE;
                                  password_pos <= password_pos - 1;
                              end
                              // TODO: else send BEL
                          end else if (password_pos != NUM_CHARS) begin
                              password_input[(((NUM_CHARS - password_pos) * 8) - 1) -: 8] <= serial_char_received;
                              dummy_password_input[(((NUM_CHARS - password_pos) * 8) - 1) -: 8] <= 8'h2A; //asterisk
                              password_pos <= password_pos + 1;
                          end
                      end
                  end
                  STATE_SEND_ID_TAP_PROMPT: begin
                      id_tap_serial_start_prompt <= 1'b1;
                      start_timer <= 1'b1;
                      timer_length <= ID_TAP_TIMEOUT;
                      state <= STATE_WAIT_ID_TAP;
                  end
                  STATE_WAIT_ID_TAP: begin
                      id_tap_enable <= 1'b1;
                      id_tap_serial_start_prompt <= 1'b0;
                      if (timer_expired) begin
                          `main_fsm__reset
                      end else if (card_data != default_card_data) begin
                          state <= STATE_ID_NUMBER;
                          id_tapped_enable <= 1'b1;
                          id_tap_enable <= 1'b0;
                          start_timer <= 1'b1;
                          timer_length <= ID_NUMBER_TIMEOUT;
                          id_num_serial_start_prompt <= 1'b1;
                      end else begin
                          start_timer <= 1'b0;
                      end
                  end
                  STATE_ID_NUMBER: begin
                      id_tapped_enable <= 1'b1;
                      id_number_enable <= 1'b1;
                      start_timer <= 1'b0;
                      id_num_serial_start_prompt <= 1'b0;
                      if (timer_expired) begin
                          `main_fsm__reset
                      end else if (id_number_pos == ID_NUM_CHARS) begin
                          state <= STATE_WAIT_VALIDITY;
                          identity_check_start <= 1'b1;
                      end else if (sirc_enable) begin
                          //add ascii number prefix
                          id_number_input[(((ID_NUM_CHARS - id_number_pos) * 8) - 1) -: 8] <= {4'b0011 , sirc_command};
                          id_number_pos <= id_number_pos + 1;
                      end
                  end
                  STATE_WAIT_VALIDITY: begin
                      identity_check_start <= 1'b0;
                      if (identity_check_done) begin
                          if (authorized) begin
                              state <= STATE_AUTHORIZED;
                          end else begin
                              state <= STATE_UNAUTHORIZED;
                              start_timer <= 1'b1;
                              timer_length <= UNAUTHORIZED_TIMEOUT;
                          end
                      end
                  end
                  STATE_AUTHORIZED: begin
                      authorized_enable <= 1'b1;
                      mit_login_enable <= 1'b0;
                      username_enable <= 1'b0;
                      password_enable <= 1'b0;
                      id_tap_enable <= 1'b0;
                      id_tapped_enable <= 1'b0;
                      id_number_enable <= 1'b0;
                  end
                  STATE_UNAUTHORIZED: begin
                      unauthorized_enable <= 1'b1;
                      mit_login_enable <= 1'b0;
                      username_enable <= 1'b0;
                      username_input_enable <= 1'b0;
                      password_enable <= 1'b0;
                      id_tap_enable <= 1'b0;
                      id_tapped_enable <= 1'b0;
                      id_number_enable <= 1'b0;
                      start_timer <= 1'b0;
                      if (timer_expired) begin
                          `main_fsm__reset
                      end
                  end
             endcase
         end
    end
endmodule

`endif // MAIN_FSM_V
