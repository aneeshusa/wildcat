`ifndef PSK_DECODE_V
`define PSK_DECODE_V

`include "./src/hdl/prologue.v"


module psk_decode(
    input wire clock, reset,
    input wire input_enable,
    input wire [1:0] input_data,
    output reg output_enable = 1'b0,
    output reg output_data = 1'b0
);

    `define psk_decode__reset() begin \
        output_enable <= 1'b0; \
        output_data <= 1'b0; \
     end


    always @(posedge clock) begin
        if (reset) begin
            `psk_decode__reset
        end else begin
            if (input_enable) begin
                output_enable <= 1'b1;
                // A transition from low to high or high to low signals a 1,
                // while staying at the same level signals a 0.
                output_data <= (input_data[1] == input_data[0]) ? 1'b0 : 1'b1;
            end else begin
                output_enable <= 1'b0;
            end
        end
    end

endmodule

`endif // PSK_DECODE_V
