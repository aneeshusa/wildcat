`ifndef ADDRESS_CALC_V
`define ADDRESS_CALC_V

`include "./src/hdl/prologue.v"


module address_calc #(
    parameter NUM_CHARS = 128,
    parameter CHAR_SIZE = 32
) (
    input wire vclock,
    input wire [10:0] hcount,
    input wire [9:0] vcount,
    input wire hsync,
    input wire vsync,
    input wire [223:0] joke_data_1,
    input wire [223:0] joke_data_2,
    input wire [223:0] joke_data_3,
    input wire [63:0] username_input,
    input wire [63:0] password_input,
    input wire [71:0] id_number_input,
    input wire mit_login_enable,
    input wire username_enable,
    input wire username_input_enable,
    input wire password_enable,
    input wire id_tap_enable,
    input wire id_tapped_enable,
    input wire id_number_enable,
    input wire authorized_enable,
    input wire unauthorized_enable,
    output reg [`CLOG2(NUM_CHARS) - 1:0] char = {`CLOG2(NUM_CHARS){1'b0}},
    output reg [`CLOG2(CHAR_SIZE) - 1:0] char_row = {`CLOG2(CHAR_SIZE){1'b0}},
    output reg [`CLOG2(CHAR_SIZE) - 1:0] char_col = {`CLOG2(CHAR_SIZE){1'b0}},
    output reg hsync_out = 1'b0,
    output reg vsync_out = 1'b0
);
    localparam INDENT = 100;

    //reg [71:0]new_joke = "MIT LOGIN";
    localparam JOKE_DATA_1_TOP = 7 * CHAR_SIZE;
    localparam JOKE_DATA_1_BOTTOM = JOKE_DATA_1_TOP + CHAR_SIZE;
    localparam JOKE_DATA_1_LEFT = INDENT;

    localparam JOKE_DATA_2_TOP = 9 * CHAR_SIZE;
    localparam JOKE_DATA_2_BOTTOM = JOKE_DATA_2_TOP + CHAR_SIZE;
    localparam JOKE_DATA_2_LEFT = INDENT;


    localparam JOKE_DATA_3_TOP = 11 * CHAR_SIZE;
    localparam JOKE_DATA_3_BOTTOM = JOKE_DATA_3_TOP + CHAR_SIZE;
    localparam JOKE_DATA_3_LEFT = INDENT;

    reg [71:0] mit_login = "MIT LOGIN";
    localparam MIT_LOGIN_TOP = 3 * CHAR_SIZE;
    localparam MIT_LOGIN_BOTTOM = MIT_LOGIN_TOP + CHAR_SIZE;
    localparam MIT_LOGIN_LEFT = INDENT;

    reg [71:0] username = "Username:";
    localparam USERNAME_TOP = 5 * CHAR_SIZE;
    localparam USERNAME_BOTTOM = USERNAME_TOP + CHAR_SIZE;
    localparam USERNAME_LEFT = INDENT;

    localparam USERNAME_INPUT_TOP = USERNAME_TOP;
    localparam USERNAME_INPUT_BOTTOM = USERNAME_BOTTOM;
    localparam USERNAME_INPUT_LEFT = USERNAME_LEFT + (9 * CHAR_SIZE);

    reg [71:0] password = "Password:";
    localparam PASSWORD_TOP = 7 * CHAR_SIZE;
    localparam PASSWORD_BOTTOM = PASSWORD_TOP + CHAR_SIZE;
    localparam PASSWORD_LEFT = INDENT;

    localparam PASSWORD_INPUT_TOP = PASSWORD_TOP;
    localparam PASSWORD_INPUT_BOTTOM = PASSWORD_BOTTOM;
    localparam PASSWORD_INPUT_LEFT = PASSWORD_LEFT + (9 * CHAR_SIZE);


    reg [111:0] id_tap = "Please tap ID.";
    localparam ID_TAP_TOP = 9 * CHAR_SIZE;
    localparam ID_TAP_BOTTOM = ID_TAP_TOP + CHAR_SIZE;
    localparam ID_TAP_LEFT = INDENT;

    reg [79:0] id_tapped = "ID tapped.";
    localparam ID_TAPPED_TOP = 9 * CHAR_SIZE;
    localparam ID_TAPPED_BOTTOM = ID_TAPPED_TOP + CHAR_SIZE;
    localparam ID_TAPPED_LEFT = INDENT;

    reg [71:0] input_id = "Input ID:";
    localparam INPUT_ID_TOP = 11 * CHAR_SIZE;
    localparam INPUT_ID_BOTTOM = INPUT_ID_TOP + CHAR_SIZE;
    localparam INPUT_ID_LEFT = INDENT;

    localparam ID_NUMBER_INPUT_TOP = INPUT_ID_TOP;
    localparam ID_NUMBER_INPUT_BOTTOM = INPUT_ID_BOTTOM;
    localparam ID_NUMBER_INPUT_LEFT = INDENT + (9 * CHAR_SIZE);

    reg [79:0] authorized = "AUTHORIZED";
    localparam AUTHORIZED_TOP = MIT_LOGIN_TOP;
    localparam AUTHORIZED_BOTTOM = AUTHORIZED_TOP + CHAR_SIZE;
    localparam AUTHORIZED_LEFT = INDENT;

    reg [55:0] welcome = "WELCOME";
    localparam WELCOME_TOP = 5 * CHAR_SIZE;
    localparam WELCOME_BOTTOM = WELCOME_TOP + CHAR_SIZE;
    localparam WELCOME_LEFT = INDENT + CHAR_SIZE;

    reg [95:0] unauthorized = "UNAUTHORIZED";
    localparam UNAUTHORIZED_TOP = MIT_LOGIN_TOP;
    localparam UNAUTHORIZED_BOTTOM = UNAUTHORIZED_TOP + CHAR_SIZE;
    localparam UNAUTHORIZED_LEFT = INDENT;

    integer current_loc_joke_data_1;
    integer current_loc_joke_data_2;
    integer current_loc_joke_data_3;
    integer current_loc_mit_login;
    integer current_loc_username;
    integer current_loc_username_input;
    integer current_loc_password;
    integer current_loc_password_input;
    integer current_loc_id_tap;
    integer current_loc_id_tapped;
    integer current_loc_input_id;
    integer current_loc_id_number_input;
    integer current_loc_authorized;
    integer current_loc_welcome;
    integer current_loc_unauthorized;
    reg hsync_buf, vsync_buf;


    always @(posedge vclock) begin
        //joke display
        for (current_loc_joke_data_1 = 0 ; current_loc_joke_data_1 < 28 ;
             current_loc_joke_data_1 = current_loc_joke_data_1 + 1) begin
            if (
                (hcount > (JOKE_DATA_1_LEFT + (current_loc_joke_data_1*CHAR_SIZE))) &&
                (hcount < (JOKE_DATA_1_LEFT + ((current_loc_joke_data_1 + 1)*CHAR_SIZE))) &&
                (vcount > JOKE_DATA_1_TOP) && (vcount < JOKE_DATA_1_BOTTOM) && (authorized_enable)
            ) begin
                   char <= joke_data_1[(((28 - current_loc_joke_data_1) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end
        for (current_loc_joke_data_2 = 0 ; current_loc_joke_data_2 < 28 ;
             current_loc_joke_data_2 = current_loc_joke_data_2 + 1) begin
            if (
                (hcount > (JOKE_DATA_2_LEFT + (current_loc_joke_data_2*CHAR_SIZE))) &&
                (hcount < (JOKE_DATA_2_LEFT + ((current_loc_joke_data_2 + 1)*CHAR_SIZE))) &&
                (vcount > JOKE_DATA_2_TOP) && (vcount < JOKE_DATA_2_BOTTOM) && (authorized_enable)
            ) begin
                   char <= joke_data_2[(((28 - current_loc_joke_data_2) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end
        for (current_loc_joke_data_3 = 0 ; current_loc_joke_data_3 < 28 ;
             current_loc_joke_data_3 = current_loc_joke_data_3 + 1) begin
            if (
                (hcount > (JOKE_DATA_3_LEFT + (current_loc_joke_data_3*CHAR_SIZE))) &&
                (hcount < (JOKE_DATA_3_LEFT + ((current_loc_joke_data_3 + 1)*CHAR_SIZE))) &&
                (vcount > JOKE_DATA_3_TOP) && (vcount < JOKE_DATA_3_BOTTOM) && (authorized_enable)
            ) begin
                   char <= joke_data_3[(((28 - current_loc_joke_data_3) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end

        // MIT LOGIN display
        for (current_loc_mit_login = 0 ; current_loc_mit_login < 9 ;
             current_loc_mit_login = current_loc_mit_login + 1) begin
            if (
                (hcount > (MIT_LOGIN_LEFT + (current_loc_mit_login*CHAR_SIZE))) &&
                (hcount < (MIT_LOGIN_LEFT + ((current_loc_mit_login + 1)*CHAR_SIZE))) &&
                (vcount > MIT_LOGIN_TOP) && (vcount < MIT_LOGIN_BOTTOM) && (mit_login_enable)
            ) begin
                   char <= mit_login[(((9 - current_loc_mit_login) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end

        // Username display
        for (current_loc_username = 0 ; current_loc_username < 9 ;
             current_loc_username = current_loc_username + 1) begin
            if (
                (hcount > (USERNAME_LEFT + (current_loc_username*CHAR_SIZE))) &&
                (hcount < (USERNAME_LEFT + ((current_loc_username + 1)*CHAR_SIZE))) &&
                (vcount > USERNAME_TOP) && (vcount < USERNAME_BOTTOM) && (username_enable)
            ) begin
                   char <= username[(((9 - current_loc_username) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end

        // Username input display
        for (current_loc_username_input = 0 ; current_loc_username_input < 8 ;
             current_loc_username_input = current_loc_username_input + 1) begin
            if (
                (hcount > (USERNAME_INPUT_LEFT + (current_loc_username_input*CHAR_SIZE))) &&
                (hcount < (USERNAME_INPUT_LEFT + ((current_loc_username_input + 1)*CHAR_SIZE))) &&
                (vcount > USERNAME_INPUT_TOP) && (vcount < USERNAME_INPUT_BOTTOM) && (username_input_enable)
            ) begin
                   char <= username_input[(((8 - current_loc_username_input) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end

        // Password display
        for (current_loc_password = 0 ; current_loc_password < 9 ;
             current_loc_password = current_loc_password + 1) begin
            if (
                (hcount > (PASSWORD_LEFT + (current_loc_password*CHAR_SIZE))) &&
                (hcount < (PASSWORD_LEFT + ((current_loc_password + 1)*CHAR_SIZE))) &&
                (vcount > PASSWORD_TOP) && (vcount < PASSWORD_BOTTOM) && (password_enable)
            ) begin
                   char <= password[(((9 - current_loc_password) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end

        // Password input display
        for (current_loc_password_input = 0 ; current_loc_password_input < 8 ;
             current_loc_password_input = current_loc_password_input + 1) begin
            if (
                (hcount > (PASSWORD_INPUT_LEFT + (current_loc_password_input*CHAR_SIZE))) &&
                (hcount < (PASSWORD_INPUT_LEFT + ((current_loc_password_input + 1)*CHAR_SIZE))) &&
                (vcount > PASSWORD_INPUT_TOP) && (vcount < PASSWORD_INPUT_BOTTOM) && (password_enable)
            ) begin
                   char <= password_input[(((8 - current_loc_password_input) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end

        // Please tap ID display
        for (current_loc_id_tap = 0 ; current_loc_id_tap < 14 ;
             current_loc_id_tap = current_loc_id_tap + 1) begin
            if (
                (hcount > (ID_TAP_LEFT + (current_loc_id_tap*CHAR_SIZE))) &&
                (hcount < (ID_TAP_LEFT + ((current_loc_id_tap + 1)*CHAR_SIZE))) &&
                (vcount > ID_TAP_TOP) && (vcount < ID_TAP_BOTTOM) && (id_tap_enable)
            ) begin
                   char <= id_tap[(((14 - current_loc_id_tap) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end

        //ID Tapped Display
        for (current_loc_id_tapped = 0 ; current_loc_id_tapped < 10 ;
             current_loc_id_tapped = current_loc_id_tapped + 1) begin
            if (
                (hcount > (ID_TAPPED_LEFT + (current_loc_id_tapped*CHAR_SIZE))) &&
                (hcount < (ID_TAPPED_LEFT + ((current_loc_id_tapped + 1)*CHAR_SIZE))) &&
                (vcount > ID_TAPPED_TOP) && (vcount < ID_TAPPED_BOTTOM) && (id_tapped_enable)
            ) begin
                   char <= id_tapped[(((10 - current_loc_id_tapped) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end

        // ID Number Input display
        for (current_loc_input_id = 0 ; current_loc_input_id < 9 ;
             current_loc_input_id = current_loc_input_id + 1) begin
            if (
                (hcount > (INPUT_ID_LEFT + (current_loc_input_id*CHAR_SIZE))) &&
                (hcount < (INPUT_ID_LEFT + ((current_loc_input_id + 1)*CHAR_SIZE))) &&
                (vcount > INPUT_ID_TOP) && (vcount < INPUT_ID_BOTTOM) && (id_number_enable)
            ) begin
                   char <= input_id[(((9 - current_loc_input_id) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end

        // ID Number Input display
        for (current_loc_id_number_input = 0 ; current_loc_id_number_input < 9 ;
             current_loc_id_number_input = current_loc_id_number_input + 1) begin
            if (
                (hcount > (ID_NUMBER_INPUT_LEFT + (current_loc_id_number_input*CHAR_SIZE))) &&
                (hcount < (ID_NUMBER_INPUT_LEFT + ((current_loc_id_number_input + 1)*CHAR_SIZE))) &&
                (vcount > ID_NUMBER_INPUT_TOP) && (vcount < ID_NUMBER_INPUT_BOTTOM) && (id_number_enable)
            ) begin
                   char <= id_number_input[(((9 - current_loc_id_number_input) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end


        // Authorized
        for (current_loc_authorized = 0 ; current_loc_authorized < 10 ;
             current_loc_authorized = current_loc_authorized + 1) begin
            if (
                (hcount > (AUTHORIZED_LEFT + (current_loc_authorized*CHAR_SIZE))) &&
                (hcount < (AUTHORIZED_LEFT + ((current_loc_authorized + 1)*CHAR_SIZE))) &&
                (vcount > AUTHORIZED_TOP) && (vcount < AUTHORIZED_BOTTOM) && (authorized_enable)
            ) begin
                   char <= authorized[(((10 - current_loc_authorized) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end

        //Welcome
        for (current_loc_welcome = 0 ; current_loc_welcome < 7 ;
             current_loc_welcome = current_loc_welcome + 1) begin
            if (
                (hcount > (WELCOME_LEFT + (current_loc_welcome*CHAR_SIZE))) &&
                (hcount < (WELCOME_LEFT + ((current_loc_welcome + 1)*CHAR_SIZE))) &&
                (vcount > WELCOME_TOP) && (vcount < WELCOME_BOTTOM) && (authorized_enable)
            ) begin
                   char <= welcome[(((7 - current_loc_welcome) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end



        // Unauthorized
        for (current_loc_unauthorized = 0 ; current_loc_unauthorized < 12 ;
             current_loc_unauthorized = current_loc_unauthorized + 1) begin
            if (
                (hcount > (UNAUTHORIZED_LEFT + (current_loc_unauthorized*CHAR_SIZE))) &&
                (hcount < (UNAUTHORIZED_LEFT + ((current_loc_unauthorized + 1)*CHAR_SIZE))) &&
                (vcount > UNAUTHORIZED_TOP) && (vcount < UNAUTHORIZED_BOTTOM) && (unauthorized_enable)
            ) begin
                   char <= unauthorized[(((12 - current_loc_unauthorized) * 8) - 1) -: 8];
                   char_row <= vcount[(`CLOG2(NUM_CHARS) - 1):0];
                   char_col <= hcount[(`CLOG2(NUM_CHARS) - 1):0];
            end
        end

        // Need to delay by 1 cycle to match calculation latency
        hsync_out <= hsync;
        vsync_out <= vsync;
    end

endmodule

`endif // ADDRESS_CALC_V
