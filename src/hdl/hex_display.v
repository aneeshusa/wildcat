`ifndef HEX_DISPLAY_V
`define HEX_DISPLAY_V

`include "./src/hdl/prologue.v"


// Display 8 hex numbers on the Nexys4 7 segment displays
// Uses Time Division Multiplexing to cut down on FPGA pin usage
module hex_display #(
    // Number of clock cycles to illuminate each display in sequence.
    // clock rate / `REFRESH_PERIOD` = refresh rate,
    // default is 60Hz refresh @ 100MHz clock (about 16ms per period)
    // Each display will be on for 1/8 of that time.
    parameter REFRESH_PERIOD = 1600000
) (
    input wire clock, reset,
    input wire [31:0] data, // 8 hex numbers, msb first
    output reg [6:0] display = {7{1'b0}},
    // digit strobe, signals current channel to FPGA
    output reg [7:0] strobe = {8{1'b1}}
);

    // Time Division Multiplexing:
    // Each channel is on for 1/8 of the REFRESH_PERIOD
    localparam MAX_COUNT = REFRESH_PERIOD / 8;
    localparam REFRESH_WIDTH = `CLOG2(MAX_COUNT);
    reg [REFRESH_WIDTH - 1:0] counter = {REFRESH_WIDTH{1'b0}};
    reg [2:0] channel = 3'b000;

    wire [6:0] segments[15:0];
    assign segments[0]  = 7'b100_0000;
    assign segments[1]  = 7'b111_1001;
    assign segments[2]  = 7'b010_0100;
    assign segments[3]  = 7'b011_0000;
    assign segments[4]  = 7'b001_1001;
    assign segments[5]  = 7'b001_0010;
    assign segments[6]  = 7'b000_0010;
    assign segments[7]  = 7'b111_1000;
    assign segments[8]  = 7'b000_0000;
    assign segments[9]  = 7'b001_1000;
    assign segments[10] = 7'b000_1000;
    assign segments[11] = 7'b000_0011;
    assign segments[12] = 7'b010_0111;
    assign segments[13] = 7'b010_0001;
    assign segments[14] = 7'b000_0110;
    assign segments[15] = 7'b000_1110;


    `define hex_display__reset() begin \
        counter <= {REFRESH_WIDTH{1'b0}}; \
        channel <= 3'b000; \
        display <= {7{1'b0}}; \
        strobe <= {8{1'b1}}; \
    end

    wire [7:0] int = 8'b0000_0001 << channel;

    always @(posedge clock) begin
        if (reset) begin
            `hex_display__reset
        end else begin
            if (counter < MAX_COUNT - 1) begin
                counter <= counter + 1;
            end else begin
                counter <= {REFRESH_WIDTH{1'b0}};
                channel <= channel + 1;
            end
            display <= segments[data[(4 * channel) + 3 -: 4]];
            strobe <= ~int;
        end
    end

endmodule

`endif // HEX_DISPLAY_V
