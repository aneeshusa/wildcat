`ifndef SIRC_NUM_CONVERSION_V
`define SIRC_NUM_CONVERSION_V

`include "./src/hdl/prologue.v"


module sirc_num_conversion(
    input wire clock,
    input wire reset,
    input wire sirc_enable,
    input wire [3:0] sirc_command,
    output reg new_sirc_enable,
    output reg [3:0] new_sirc_command
);

    always @(posedge clock) begin
        if (sirc_enable) begin
            new_sirc_enable <= 1'b1;
            case ( sirc_command )
                0: new_sirc_command <= 1;
                1: new_sirc_command <= 2;
                2: new_sirc_command <= 3;
                3: new_sirc_command <= 4;
                4: new_sirc_command <= 5;
                5: new_sirc_command <= 6;
                6: new_sirc_command <= 7;
                7: new_sirc_command <= 8;
                8: new_sirc_command <= 9;
                9: new_sirc_command <= 0;
            endcase
        end else begin
            new_sirc_enable <= 1'b0;
        end
    end
endmodule

`endif // SIRC_NUM_CONVERSION_V
