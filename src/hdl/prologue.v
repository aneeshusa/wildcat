`ifndef CUSTOM_VERILOG_PROLOGUE_H
`define CUSTOM_VERILOG_PROLOGUE_H

// Common settings and macros
// To be included at the start of every module

`default_nettype none
`timescale 1ns / 1ps

// POLYFILLS

// Go up to ~100,000,000 because the clock is 100Mhz,
// so we shouldn't need any numbers bigger than that.
`define CLOG2(x) (\
    (x <= 2) ? 1 : \
    (x <= 4) ? 2 : \
    (x <= 8) ? 3 : \
    (x <= 16) ? 4 : \
    (x <= 32) ? 5 : \
    (x <= 64) ? 6 : \
    (x <= 128) ? 7 : \
    (x <= 256) ? 8 : \
    (x <= 512) ? 9 : \
    (x <= 1024) ? 10 : \
    (x <= 2048) ? 11 : \
    (x <= 4096) ? 12 : \
    (x <= 8192) ? 13 : \
    (x <= 16384) ? 14 : \
    (x <= 32768) ? 15 : \
    (x <= 65536) ? 16 : \
    (x <= 131072) ? 17 : \
    (x <= 262144) ? 18 : \
    (x <= 524288) ? 19 : \
    (x <= 1048576) ? 20 : \
    (x <= 2097152) ? 21 : \
    (x <= 4194304) ? 22 : \
    (x <= 8388608) ? 23 : \
    (x <= 16777216) ? 24 : \
    (x <= 33554432) ? 25 : \
    (x <= 67108864) ? 26 : \
    (x <= 134217728) ? 27 : \
    28 \
)

// UTILS

`define endian_swap_32(value) ({ \
    value[7 -: 8], \
    value[15 -: 8], \
    value[23 -: 8], \
    value[31 -: 8] \
})

`define endian_swap_64(value) ({ \
    value[7 -: 8], \
    value[15 -: 8], \
    value[23 -: 8], \
    value[31 -: 8], \
    value[39 -: 8], \
    value[47 -: 8], \
    value[55 -: 8], \
    value[63 -: 8] \
})

`define leftrotate(x, c) ( ((x) << (c)) | ((x) >> (32 - c)) )

// Get a signal (of possibly unknown size) at a signal width,
// either left-filling zeros to extend, or truncating twoards LSB.
`define with_width(width, value) ({{(width){1'b0}}, value}[(width) - 1:0])

// TESTING

// Testbench timing (in nanoseconds)
`define CLK (10)

`define assert(signal, expected) begin \
    if (signal !== expected) begin \
        $display( \
            "[ FAIL ] ASSERTION in %m at %t: %h !== %h", \
            $time, signal, expected \
        ); \
        #(2 * `CLK); // Run a bit longer for nice graphs \
        $stop(); \
    end else begin \
        $display( \
            "[ PASS ] ASSERTION in %m at %t: %h === %h", \
            $time, signal, expected \
        ); \
    end \
end


`define pulse(signal, value) begin \
    signal <= value; \
    #(`CLK); \
    signal <= ~value; \
end


`define start_test(description) begin \
    #(5 * `CLK); \
    $display(""); \
    $display("[ INFO ] Starting test at %t: %s", $time, description); \
end

`endif // CUSTOM_VERILOG_PROLOGUE_H
