`ifndef TIMER_V
`define TIMER_V

`include "./src/hdl/prologue.v"

`include "./src/hdl/divider.v"


// A countdown timer.
// When start is asserted,
// starts counting down from length (which is in units of seconds),
// asserting expired (firing) when the timer runs out.
// Handles zero length requests by firing one cycle after `start` is asserted.
module timer #(
    // Frequency of `clock` aka number of cycles per second
    parameter CLOCK_FREQUENCY = 100_000_000,
    // Maximum timer length (in seconds) to support
    parameter MAX_TIMER_LENGTH = 16
) (
    input wire clock, reset,
    input wire start,
    input wire [`CLOG2(MAX_TIMER_LENGTH + 1) - 1:0] timer_length,
    output reg expired = 1'b0
);

    localparam STATE_IDLE = 1'b0;
    localparam STATE_RUNNING = 1'b1;

    localparam DEFAULT_STATE = STATE_IDLE;
    reg state = DEFAULT_STATE;

    // NOTE: not the same width as `timer_length`
    // because `count` stores at most 1 less than `timer_length`
    localparam COUNT_WIDTH = `CLOG2(MAX_TIMER_LENGTH);
    localparam DEFAULT_COUNT = {COUNT_WIDTH{1'b0}};
    reg [COUNT_WIDTH - 1:0] count = DEFAULT_COUNT;

    // Need to reset divider if a global reset is given,
    // as well as whenever we start the timer.
    wire divider_reset = reset || start;
    wire one_hz_enable;
    divider #(
        .PERIOD(CLOCK_FREQUENCY)
    ) one_hz(
        .clock(clock), .reset(divider_reset),
        .enable(one_hz_enable)
    );


    `define timer__reset() begin \
        state <= DEFAULT_STATE; \
        count <= DEFAULT_COUNT; \
        expired <= 1'b0; \
    end


    `define timer__start() begin \
        if (timer_length > {COUNT_WIDTH{1'b0}}) begin \
            // Start the timer! \
            state <= STATE_RUNNING; \
            count <= timer_length - 1; \
            expired <= 1'b0; \
        end else begin \
            // No need to start timer for length 0, \
            // the timer will fire immediately (next cycle) in this case. \
            // Just go back to IDLE immediately. \
            state <= STATE_IDLE; \
            expired <= 1'b1; \
        end \
    end


    always @(posedge clock) begin
        if (reset) begin
            `timer__reset
        end else begin
            // Allow restarts even if already running,
            // to avoid requiring the timer to be IDLE.
            // (That would require an additional cycle for a reset.)
            if (start) begin
                `timer__start
            end else begin
                case (state)
                    STATE_IDLE: begin
                        expired <= 1'b0;
                    end
                    STATE_RUNNING: begin
                        if (one_hz_enable) begin
                            if (count > {COUNT_WIDTH{1'b0}}) begin
                                count <= count - 1;
                            end else begin
                                expired <= 1'b1;
                                state <= STATE_IDLE;
                            end
                        end
                    end
                    default: begin
                        `timer__reset
                    end
                endcase
            end
        end
    end

endmodule

`endif // TIMER_V
