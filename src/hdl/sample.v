`ifndef SAMPLE_V
`define SAMPLE_V

`include "./src/hdl/prologue.v"


// Samples an input wire each cycle that `sample_enable` is asserted.
// Provides a buffered output wire as well as an enable signal.
module sample (
    input wire clock, reset,
    input wire sample_enable, data_in,
    output reg output_enable = 1'b0,
    output reg data_out = 1'b0
);

    `define sample__do_reset() begin \
        output_enable <= 1'b0; \
        data_out <= 1'b0; \
    end

    always @(posedge clock) begin
        if (reset) begin
            `sample__do_reset
        end else begin
            if (sample_enable == 1'b1) begin
                data_out <= data_in;
                output_enable <= 1'b1;
            end else begin
                output_enable <= 1'b0;
            end
        end
    end

endmodule

`endif // SAMPLE_V
