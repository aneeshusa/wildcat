`ifndef DOWNSAMPLE_V
`define DOWNSAMPLE_V

`include "./src/hdl/prologue.v"


// Downsamples a 1 bit digital input by `DOWNSAMPLE_RATIO`.
// Currently uses a crude/simple implementation of majority wins,
// and always outputs a value on `schedule`
// (e.g. does not punt or delay in case of mixed inputs).
//
// Timing:
// - Resets take one clock cycle
// - One sample is taken on each input_enable
// - Downsampled data is available one cycle after last sample is taken,
//   where last === DOWNSAMPLE_RATIO
module downsample #(
    parameter DOWNSAMPLE_RATIO = 8
) (
    input wire clock, reset,
    input wire input_enable, data_in,
    // Asserted for one cycle when a new downsampled data bit
    // is available for consumption.
    // Single-cycle output_enable allows easy triggering on new data.
    output reg output_enable = 1'b0,
    // Downsampled output bit; continuously buffered.
    output reg data_out = 1'b0
);

    localparam SAMPLING_WIDTH = `CLOG2(DOWNSAMPLE_RATIO);
    // How many samples (bits) have we processed so far in this round
    reg [SAMPLING_WIDTH - 1:0] sample_count = {SAMPLING_WIDTH{1'b0}};
    // How many of those bits were ones
    reg [SAMPLING_WIDTH - 1:0] num_ones = {SAMPLING_WIDTH{1'b0}};


    `define downsample__restart() begin \
        sample_count <= {SAMPLING_WIDTH{1'b0}}; \
        num_ones <= {SAMPLING_WIDTH{1'b0}}; \
    end


    `define downsample__reset() begin \
        `downsample__restart \
        data_out <= 1'b0; \
        output_enable <= 1'b0; \
    end


    always @(posedge clock) begin
        if (reset) begin
            `downsample__reset
        end else begin
            if (input_enable) begin
                if (sample_count < DOWNSAMPLE_RATIO - 1) begin
                    sample_count <= sample_count + 1;
                    num_ones <= num_ones + data_in;
                    output_enable <= 1'b0;
                end else begin
                    `downsample__restart
                    // Current implementation is crude and always signals a
                    // valid output bit after DOWNSAMPLE_RATIO samples.
                    // Ties are broken in favor of outputting a high value.
                    data_out <= (
                        (num_ones + data_in) >= (DOWNSAMPLE_RATIO / 2)
                    );
                    output_enable <= 1'b1;
                end
            end else begin
                output_enable <= 1'b0;
            end
        end
    end

endmodule

`endif // DOWNSAMPLE_V
