`ifndef EDGE_DETECTOR_V
`define EDGE_DETECTOR_V

`include "./src/hdl/prologue.v"

`define POSEDGE (2'b01)
`define NEGEDGE (2'b10)


// Detects edge transition (between clock edges) on some input signal.
module edge_detector #(
    // Pattern to match, use one of `POSEDGE or `NEGEDGE
    parameter PATTERN = `POSEDGE
) (
    input wire clock, reset,
    input wire data_in,
    output reg data_out = 1'b0
);

    reg started = 1'b0;
    reg last_seen = 1'b0;

    always @(posedge clock) begin
        if (reset) begin
            data_out <= 1'b0;
            started <= 1'b0;
            last_seen <= 1'b0;
        end else begin
            started <= 1'b1;
            last_seen <= data_in;
            if (started == 1'b1) begin
                data_out <= ({last_seen,data_in} == PATTERN) ? 1 : 0;
            end
        end
    end

endmodule

`endif // EDGE_DETECTOR_V
