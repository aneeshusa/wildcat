`ifndef JOKE_DATABASE_V
`define JOKE_DATABASE_V

`include "./src/hdl/prologue.v"

module joke_database (
    input wire clock,
    input wire read_enable,
    output reg [223:0] joke_data_1,
    output reg [223:0] joke_data_2,
    output reg [223:0] joke_data_3
);

    reg [2:0] count = 3'b000;
    always @(posedge clock) begin
        if (read_enable) begin
            case(count)
                0: begin
                    joke_data_1 = "Q: Anyone know any jokes    ";
                    joke_data_2 = "about sodium?               ";
                    joke_data_3 = "A: Na                       ";
                end
                1: begin
                    joke_data_1 = "Are you course 6?           ";
                    joke_data_2 = "Cuz you're ir-resistor-able!";
                    joke_data_3 = "                            ";
                end
                2: begin
                    joke_data_1 = "Do you know the name Pavlov?";
                    joke_data_2 = "It rings a bell.            ";
                    joke_data_3 = "                            ";
                end
                3: begin
                    joke_data_1 = "Why did the programmer quit ";
                    joke_data_2 = "quit his job?  He didn't    ";
                    joke_data_3 = "get arrays.                 ";
                end
                4: begin
                    joke_data_1 = "2 antennas got married. The ";
                    joke_data_2 = "wedding was lousy but the   ";
                    joke_data_3 = "reception was outstanding!  ";
                end
                5: begin
                    joke_data_1 = "Red wire to black wire: 'Why";
                    joke_data_2 = "are you sad?' The black wire";
                    joke_data_3 = "replied:'I've been grounded'";
                end
                6: begin
                    joke_data_1 = "To survive as a power supply";
                    joke_data_2 = "designer you need 2 things: ";
                    joke_data_3 = "A technical background & CPR";
                end
                7: begin
                    joke_data_1 = "There are 10 kinds of people";
                    joke_data_2 = "in this world:those who know";
                    joke_data_3 = "binary and those who don't. ";
                end
                8: begin
                    joke_data_1 = "Q: What did the electrical  ";
                    joke_data_2 = "engineer say when he got    ";
                    joke_data_3 = "shocked?  A: That hertz.    ";
                end
            endcase
        end else begin
            count <= count + 1;
        end
    end

    
endmodule

`endif // JOKE_DATABASE_V
