`ifndef DESCRAMBLE_FLEXSECURE_V
`define DESCRAMBLE_FLEXSECURE_V

`include "./src/hdl/prologue.v"


// Indala FlexSecure protection scheme defeater.
// Algorithm taken from
// http://web.mit.edu/keithw/Public/MIT-Card-Vulnerabilities-March31.pdf.
module descramble_flexsecure #(
    parameter KEY = 32'b0000_0000_0000_0000_0000_0000_0000_0000
) (
    input wire clock, reset,
    input wire prox_enable,
    input wire [31:0] prox_data,
    output reg out_enable = 1'b0,
    output reg [31:0] out_data = {32{1'b0}}
);

    // Intermediate results in algorithm
    wire xor_bit = prox_data[20] ^ prox_data[26];
    wire [31:0] reassigned = {
        prox_data[25],
        prox_data[25],
        prox_data[31:27],
        prox_data[24:21],
        xor_bit,
        prox_data[19:0]
    };


    `define descramble_flexsecure__reset() begin \
        out_enable <= 1'b0; \
        out_data <= {32{1'b0}}; \
    end


    always @(posedge clock) begin
        if (reset) begin
            `descramble_flexsecure__reset
        end else begin
            if (prox_enable) begin
                out_enable <= 1'b1;
                out_data <= reassigned ^ KEY;
            end else begin
                out_enable <= 1'b0;
            end
        end
    end

endmodule

`endif // DESCRAMBLE_FLEXSECURE_V
