`ifndef SERIAL_PROMPT_V
`define SERIAL_PROMPT_V

`include "./src/hdl/prologue.v"


module username_serial_prompt(
    input wire clock,
    input wire reset,
    input wire done,
    input wire start,
    output reg serial_send_enable,
    output reg [7:0]send_char,
    output reg [1:0]debug
);
    
    localparam USERNAME_PROMPT_LEN = 21;
    reg [(8 * USERNAME_PROMPT_LEN)-1:0] username_prompt = {"Enter your username: "};
    reg [`CLOG2(USERNAME_PROMPT_LEN) -1:0] username_pos;

    reg state = 1'b0;
    always @(posedge clock) begin
        debug <= state;
        if (reset) begin
            state <= 1'b0;
            username_pos <= {`CLOG2(USERNAME_PROMPT_LEN){1'b0}};
        end else begin
            case (state)
                1'b0: begin
                    if (start) begin
                        state <= 1'b1;
                        serial_send_enable <= 1'b1;
                        username_pos <= {`CLOG2(USERNAME_PROMPT_LEN){1'b0}};
                    end else begin
                        serial_send_enable <= 1'b0;
                    end
                end
                1'b1: begin
                    // Wait to send next char
                    if (done) begin
                        if (username_pos < USERNAME_PROMPT_LEN - 1) begin
                            send_char <= username_prompt[8 * (USERNAME_PROMPT_LEN - username_pos) -1-: 8];
                            username_pos <= username_pos + 1;
                            serial_send_enable <= 1'b1;
                        end else begin
                            state <= 1'b0;
                            serial_send_enable <= 1'b0;
                        end
                    end else begin
                        serial_send_enable <= 1'b0;
                    end
                end
            endcase
        end
        
    end
endmodule

module password_serial_prompt(
    input wire clock,
    input wire reset,
    input wire done,
    input wire start,
    output reg serial_send_enable,
    output reg [7:0]send_char
);
    
    localparam PASSWORD_PROMPT_LEN = 22;
    reg [(8 * PASSWORD_PROMPT_LEN)-1:0] password_prompt = {8'h0D, "Enter your password: "};
    reg [`CLOG2(PASSWORD_PROMPT_LEN) -1:0] password_pos;

    reg state = 1'b0;
    always @(posedge clock) begin
        if (reset) begin
            state <= 1'b0;
            password_pos <= {`CLOG2(PASSWORD_PROMPT_LEN){1'b0}};
        end else begin
            case (state)
                1'b0: begin
                    if (start) begin
                        state <= 1'b1;
                        serial_send_enable <= 1'b1;
                        password_pos <= {`CLOG2(PASSWORD_PROMPT_LEN){1'b0}};
                    end else begin
                        serial_send_enable <= 1'b0;
                    end
                end
                1'b1: begin
                    // Wait to send next char
                    if (done) begin
                        if (password_pos < PASSWORD_PROMPT_LEN - 1) begin
                            send_char <= password_prompt[8 * (PASSWORD_PROMPT_LEN - password_pos) -1-: 8];
                            password_pos <= password_pos + 1;
                            serial_send_enable <= 1'b1;
                        end else begin
                            state <= 1'b0;
                            serial_send_enable <= 1'b0;
                        end
                    end else begin
                        serial_send_enable <= 1'b0;
                    end
                end
            endcase
        end
        
    end
endmodule

module id_tap_serial_prompt(
    input wire clock,
    input wire reset,
    input wire done,
    input wire start,
    output reg serial_send_enable,
    output reg [7:0]send_char
);
    
    localparam ID_TAP_PROMPT_LEN = 14;
    reg [(8 * ID_TAP_PROMPT_LEN)-1:0] id_tap_prompt = {8'h0D, "Tap your ID. "};
    reg [`CLOG2(ID_TAP_PROMPT_LEN) -1:0] id_tap_pos;

    reg state = 1'b0;
    always @(posedge clock) begin
        if (reset) begin
            state <= 1'b0;
            id_tap_pos <= {`CLOG2(ID_TAP_PROMPT_LEN){1'b0}};
        end else begin
            case (state)
                1'b0: begin
                    if (start) begin
                        state <= 1'b1;
                        serial_send_enable <= 1'b1;
                        id_tap_pos <= {`CLOG2(ID_TAP_PROMPT_LEN){1'b0}};
                    end else begin
                        serial_send_enable <= 1'b0;
                    end
                end
                1'b1: begin
                    // Wait to send next char
                    if (done) begin
                        if (id_tap_pos < ID_TAP_PROMPT_LEN - 1) begin
                            send_char <= id_tap_prompt[8 * (ID_TAP_PROMPT_LEN - id_tap_pos) -1-: 8];
                            id_tap_pos <= id_tap_pos + 1;
                            serial_send_enable <= 1'b1;
                        end else begin
                            state <= 1'b0;
                            serial_send_enable <= 1'b0;
                        end
                    end else begin
                        serial_send_enable <= 1'b0;
                    end
                end
            endcase
        end
        
    end
endmodule



module id_num_serial_prompt(
    input wire clock,
    input wire reset,
    input wire done,
    input wire start,
    output reg serial_send_enable,
    output reg [7:0]send_char
);
    
    localparam ID_NUM_PROMPT_LEN = 18;
    reg [(8 * ID_NUM_PROMPT_LEN)-1:0] id_num_prompt = {8'h0D, "Input ID number. "};
    reg [`CLOG2(ID_NUM_PROMPT_LEN) -1:0] id_num_pos;

    reg state = 1'b0;
    always @(posedge clock) begin
        if (reset) begin
            state <= 1'b0;
            id_num_pos <= {`CLOG2(ID_NUM_PROMPT_LEN){1'b0}};
        end else begin
            case (state)
                1'b0: begin
                    if (start) begin
                        state <= 1'b1;
                        serial_send_enable <= 1'b1;
                        id_num_pos <= {`CLOG2(ID_NUM_PROMPT_LEN){1'b0}};
                    end else begin
                        serial_send_enable <= 1'b0;
                    end
                end
                1'b1: begin
                    // Wait to send next char
                    if (done) begin
                        if (id_num_pos < ID_NUM_PROMPT_LEN - 1) begin
                            send_char <= id_num_prompt[8 * (ID_NUM_PROMPT_LEN - id_num_pos) -1-: 8];
                            id_num_pos <= id_num_pos + 1;
                            serial_send_enable <= 1'b1;
                        end else begin
                            state <= 1'b0;
                            serial_send_enable <= 1'b0;
                        end
                    end else begin
                        serial_send_enable <= 1'b0;
                    end
                end
            endcase
        end
        
    end
endmodule




`endif // SERIAL_PROMPT_V
