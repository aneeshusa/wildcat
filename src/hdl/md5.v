`ifndef MD5_V
`define MD5_V

`include "./src/hdl/prologue.v"


// Full implementation of the MD5 hashing algorithm,
// which is able to handle multiple 512-bit chunks.
// This is a batch implementation, not streaming;
// each instance must be sized for a given input width
// and the full input data must be given all at once.
module md5 #(
    parameter INPUT_WIDTH = 64 // Bits of input to accept
) (
    input wire clock, reset,
    input wire start,
    input wire [INPUT_WIDTH - 1:0] input_data,
    output reg output_enable = 1'b0,
    output reg [127:0] output_data = {128{1'b0}}
);

    // CONSTANTS (s and K)

    wire [5:0] s[63:0];
    assign s[0]  = 5'd7;
    assign s[1]  = 5'd12;
    assign s[2]  = 5'd17;
    assign s[3]  = 5'd22;
    assign s[4]  = 5'd7;
    assign s[5]  = 5'd12;
    assign s[6]  = 5'd17;
    assign s[7]  = 5'd22;
    assign s[8]  = 5'd7;
    assign s[9]  = 5'd12;
    assign s[10] = 5'd17;
    assign s[11] = 5'd22;
    assign s[12] = 5'd7;
    assign s[13] = 5'd12;
    assign s[14] = 5'd17;
    assign s[15] = 5'd22;
    assign s[16] = 5'd5;
    assign s[17] = 5'd9;
    assign s[18] = 5'd14;
    assign s[19] = 5'd20;
    assign s[20] = 5'd5;
    assign s[21] = 5'd9;
    assign s[22] = 5'd14;
    assign s[23] = 5'd20;
    assign s[24] = 5'd5;
    assign s[25] = 5'd9;
    assign s[26] = 5'd14;
    assign s[27] = 5'd20;
    assign s[28] = 5'd5;
    assign s[29] = 5'd9;
    assign s[30] = 5'd14;
    assign s[31] = 5'd20;
    assign s[32] = 5'd4;
    assign s[33] = 5'd11;
    assign s[34] = 5'd16;
    assign s[35] = 5'd23;
    assign s[36] = 5'd4;
    assign s[37] = 5'd11;
    assign s[38] = 5'd16;
    assign s[39] = 5'd23;
    assign s[40] = 5'd4;
    assign s[41] = 5'd11;
    assign s[42] = 5'd16;
    assign s[43] = 5'd23;
    assign s[44] = 5'd4;
    assign s[45] = 5'd11;
    assign s[46] = 5'd16;
    assign s[47] = 5'd23;
    assign s[48] = 5'd6;
    assign s[49] = 5'd10;
    assign s[50] = 5'd15;
    assign s[51] = 5'd21;
    assign s[52] = 5'd6;
    assign s[53] = 5'd10;
    assign s[54] = 5'd15;
    assign s[55] = 5'd21;
    assign s[56] = 5'd6;
    assign s[57] = 5'd10;
    assign s[58] = 5'd15;
    assign s[59] = 5'd21;
    assign s[60] = 5'd6;
    assign s[61] = 5'd10;
    assign s[62] = 5'd15;
    assign s[63] = 5'd21;


    wire [31:0] K[63:0];
    assign K[0]  = 32'hd76aa478;
    assign K[1]  = 32'he8c7b756;
    assign K[2]  = 32'h242070db;
    assign K[3]  = 32'hc1bdceee;
    assign K[4]  = 32'hf57c0faf;
    assign K[5]  = 32'h4787c62a;
    assign K[6]  = 32'ha8304613;
    assign K[7]  = 32'hfd469501;
    assign K[8]  = 32'h698098d8;
    assign K[9]  = 32'h8b44f7af;
    assign K[10] = 32'hffff5bb1;
    assign K[11] = 32'h895cd7be;
    assign K[12] = 32'h6b901122;
    assign K[13] = 32'hfd987193;
    assign K[14] = 32'ha679438e;
    assign K[15] = 32'h49b40821;
    assign K[16] = 32'hf61e2562;
    assign K[17] = 32'hc040b340;
    assign K[18] = 32'h265e5a51;
    assign K[19] = 32'he9b6c7aa;
    assign K[20] = 32'hd62f105d;
    assign K[21] = 32'h02441453;
    assign K[22] = 32'hd8a1e681;
    assign K[23] = 32'he7d3fbc8;
    assign K[24] = 32'h21e1cde6;
    assign K[25] = 32'hc33707d6;
    assign K[26] = 32'hf4d50d87;
    assign K[27] = 32'h455a14ed;
    assign K[28] = 32'ha9e3e905;
    assign K[29] = 32'hfcefa3f8;
    assign K[30] = 32'h676f02d9;
    assign K[31] = 32'h8d2a4c8a;
    assign K[32] = 32'hfffa3942;
    assign K[33] = 32'h8771f681;
    assign K[34] = 32'h6d9d6122;
    assign K[35] = 32'hfde5380c;
    assign K[36] = 32'ha4beea44;
    assign K[37] = 32'h4bdecfa9;
    assign K[38] = 32'hf6bb4b60;
    assign K[39] = 32'hbebfbc70;
    assign K[40] = 32'h289b7ec6;
    assign K[41] = 32'heaa127fa;
    assign K[42] = 32'hd4ef3085;
    assign K[43] = 32'h04881d05;
    assign K[44] = 32'hd9d4d039;
    assign K[45] = 32'he6db99e5;
    assign K[46] = 32'h1fa27cf8;
    assign K[47] = 32'hc4ac5665;
    assign K[48] = 32'hf4292244;
    assign K[49] = 32'h432aff97;
    assign K[50] = 32'hab9423a7;
    assign K[51] = 32'hfc93a039;
    assign K[52] = 32'h655b59c3;
    assign K[53] = 32'h8f0ccc92;
    assign K[54] = 32'hffeff47d;
    assign K[55] = 32'h85845dd1;
    assign K[56] = 32'h6fa87e4f;
    assign K[57] = 32'hfe2ce6e0;
    assign K[58] = 32'ha3014314;
    assign K[59] = 32'h4e0811a1;
    assign K[60] = 32'hf7537e82;
    assign K[61] = 32'hbd3af235;
    assign K[62] = 32'h2ad7d2bb;
    assign K[63] = 32'heb86d391;


    // REGISTERS
    // State
    localparam STATE_IDLE = 3'b000;
    localparam STATE_START_CHUNK = 3'b001;
    localparam STATE_CALCULATE_FG = 3'b010;
    localparam STATE_UPDATE_ABCD = 3'b011;
    localparam STATE_END_CHUNK = 3'b100;
    localparam STATE_FINALIZE = 3'b101;
    reg [2:0] state = STATE_IDLE;

    // Data and chunking
    // See padding algo
    localparam NUM_CHUNKS = 1 + ((INPUT_WIDTH + 1) / 512) + (
        ((INPUT_WIDTH + 1) % 512) > 448 ? 1 : 0
    );
    localparam NUM_CHUNKS_WIDTH = `CLOG2(NUM_CHUNKS);
    reg [NUM_CHUNKS_WIDTH - 1:0] chunk_counter = {NUM_CHUNKS_WIDTH{1'b0}};

    localparam DATA_WIDTH = 512 * NUM_CHUNKS; // Each chunk is 512 bits
    reg [DATA_WIDTH - 1:0] data = {DATA_WIDTH{1'b0}};
    wire [511:0] chunk = data[(512 * (NUM_CHUNKS - chunk_counter) - 1) -: 512];

    // Running overall digest values
    reg [31:0] a0 = 32'h67452301;
    reg [31:0] b0 = 32'hefcdab89;
    reg [31:0] c0 = 32'h98badcfe;
    reg [31:0] d0 = 32'h10325476;

    // Per chunk state
    // Loop Counter
    reg [5:0] i = {6{1'b0}};
    // Intermediates
    reg [31:0] F = {32{1'b0}};
    reg [3:0] g = {3{1'b0}};
    // State words
    reg [31:0] A = {32{1'b0}};
    reg [31:0] B = {32{1'b0}};
    reg [31:0] C = {32{1'b0}};
    reg [31:0] D = {32{1'b0}};

    localparam ZERO_PAD_WIDTH = (NUM_CHUNKS * 512) - 64 - (
        (INPUT_WIDTH + 1) % 512
    );

    // These variables cannot be written inline for some reason
    localparam PADDED_INPUT_WIDTH = `with_width(64, INPUT_WIDTH);
    localparam FLIPPED_PADDED_INPUT_WIDTH = `endian_swap_64(PADDED_INPUT_WIDTH);
    wire [31:0] mval = chunk[(32 * (16 - g) - 1) -: 32];


    `define md5__reset() begin \
        output_enable <= 1'b0; \
        output_data <= {128{1'b0}}; \
        state <= STATE_IDLE; \
        data <= {DATA_WIDTH{1'b0}}; \
        a0 <= 32'h67452301; \
        b0 <= 32'hefcdab89; \
        c0 <= 32'h98badcfe; \
        d0 <= 32'h10325476; \
        i <= {6{1'b0}}; \
        F <= {32{1'b0}}; \
        g <= {3{1'b0}}; \
        A <= {32{1'b0}}; \
        B <= {32{1'b0}}; \
        C <= {32{1'b0}}; \
        D <= {32{1'b0}}; \
    end


    always @(posedge clock) begin
        if (reset) begin
            `md5__reset
        end else begin
            case (state)
                STATE_IDLE: begin
                    output_enable <= 1'b0;

                    if (start) begin
                        data <= {
                            input_data,
                            1'b1,
                            {ZERO_PAD_WIDTH{1'b0}},
                            FLIPPED_PADDED_INPUT_WIDTH
                        };
                        a0 <= 32'h67452301;
                        b0 <= 32'hefcdab89;
                        c0 <= 32'h98badcfe;
                        d0 <= 32'h10325476;
                        chunk_counter <= {NUM_CHUNKS_WIDTH{1'b0}};

                        state <= STATE_START_CHUNK;
                    end
                end
                STATE_START_CHUNK: begin
                    output_enable <= 1'b0;

                    A <= a0;
                    B <= b0;
                    C <= c0;
                    D <= d0;
                    i <= {6{1'b0}};

                    state <= STATE_CALCULATE_FG;
                end
                STATE_CALCULATE_FG: begin
                    output_enable <= 1'b0;

                    if (i < 16) begin
                        F <= (B & C) | ((~B) & D);
                        g <= i % 16;
                    end else if (i < 32) begin
                        F <= (D & B) | ((~D) & C);
                        g <= ((5 * i) + 1) % 16;
                    end else if (i < 48) begin
                        F <= B ^ C ^ D;
                        g <= ((3 * i) + 5) % 16;
                    end else begin
                        F <= C ^ (B | (~D));
                        g <= (7 * i) % 16;
                    end

                    state <= STATE_UPDATE_ABCD;
                end
                STATE_UPDATE_ABCD: begin
                    output_enable <= 1'b0;

                    D <= C;
                    C <= B;
                    B <= B + `leftrotate(
                        A + F + K[i] + `endian_swap_32(mval),
                        s[i]
                    );
                    A <= D;

                    if (i < 63) begin
                        i <= i + 1;
                        state <= STATE_CALCULATE_FG;
                    end else begin
                        state <= STATE_END_CHUNK;
                    end
                end
                STATE_END_CHUNK: begin
                    output_enable <= 1'b0;

                    a0 <= a0 + A;
                    b0 <= b0 + B;
                    c0 <= c0 + C;
                    d0 <= d0 + D;

                    if (chunk_counter < NUM_CHUNKS - 1) begin
                            chunk_counter <= chunk_counter + 1;
                            state <= STATE_START_CHUNK;
                    end else begin
                        state <= STATE_FINALIZE;
                    end
                end
                STATE_FINALIZE: begin
                    output_enable <= 1'b1;
                    output_data <= {
                        `endian_swap_32(a0),
                        `endian_swap_32(b0),
                        `endian_swap_32(c0),
                        `endian_swap_32(d0)
                    };
                    state <= STATE_IDLE;
                end
            endcase
        end
    end

endmodule

`endif // MD5_V
