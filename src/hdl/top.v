`ifndef TOP_V
`define TOP_V

`include "./src/hdl/prologue.v"

`include "./src/hdl/address_calc.v"
`include "./src/hdl/char_spritemap.v"
`include "./src/hdl/debounce.v"
`include "./src/hdl/divider.v"
`include "./src/hdl/downsample.v"
`include "./src/hdl/edge_detector.v"
`include "./src/hdl/hex_display.v"
`include "./src/hdl/id_card_reader.v"
`include "./src/hdl/rs232_receive_char.v"
`include "./src/hdl/rs232_send_char.v"
`include "./src/hdl/sample.v"
`include "./src/hdl/sirc_receiver.v"
`include "./src/hdl/square_wave.v"
`include "./src/hdl/synchronize.v"
`include "./src/hdl/xvga.v"
`include "./src/hdl/main_fsm.v"
`include "./src/hdl/sirc_num_conversion.v"
`include "./src/hdl/joke_database.v"
`include "./src/hdl/serial_prompt.v"
`include "./src/hdl/serial_output_sel.v"
`include "./src/hdl/background_sel.v"
`include "./src/hdl/deduplicator_mod.v"

// Top level module for the Nexys4.
// Instantiates various modules and connects them to FPGA pins.
module nexys4(
    input wire CLK100MHZ,
    input wire BTNR,
    input wire UART_RXD,
    inout wire [2:0] JA,
    output wire [7:0] AN,
    output wire [15:0] LED,
    output wire [6:0] SEG,
    output wire UART_TXD,
    output wire [3:0] VGA_R, VGA_G, VGA_B,
    output wire VGA_HS, VGA_VS
);

    localparam NUM_SYNC = 2;
    localparam DEBOUNCE_DELAY = 650000; // 0.01 seconds

    // Bootstrapping: reset and vclock
    // The XVGA pixel clock (`vclock`) is used as the main clock domain,
    // but must be bootstrapped from the clockgen module,
    // which thus must be run in the `CLK100MHZ` domain.
    // The reset line is also needed at this time, so debounce the incoming
    // reset button signal in the `CLK100MHZ` domain as well.
    wire reset_button;
    debounce #(
        .DELAY(DEBOUNCE_DELAY)
    ) reset_button_gen(
        .clock(CLK100MHZ), .reset(reset_button),
        .noisy(BTNR),
        .clean(reset_button)
    );

    wire vclock;
    wire clocks_locked;
    clockgen clockgen_mod(
        // Only use the button for resets to avoid infinitely resetting
        // while waiting for its own output to become locked
        .clock(CLK100MHZ), .reset(reset_button),
        .vclock(vclock), .locked(clocks_locked)
    );

    // Now synchronize the reset signal to bring it into the `vclock` domain,
    // and additionally avoid starting the system until `vclock` is stable.
    wire reset_raw = reset_button || !clocks_locked;
    wire reset;
    synchronize #(
        .NUM_SYNC(NUM_SYNC)
    ) reset_gen(
        .clock(vclock), .reset(reset),
        .raw(reset_raw),
        .synchronized(reset)
    );


    wire rfid_carrier;
    square_wave #(
        .PERIOD(520) // 125 KHz
    ) rfid_carrier_gen(
        .clock(vclock), .reset(reset),
        .square_wave(rfid_carrier)
    );
    assign JA[1] = rfid_carrier;

    wire card_in;
    synchronize #(
        .NUM_SYNC(NUM_SYNC)
    ) card_in_gen(
        .clock(vclock), .reset(reset),
        .raw(JA[0]),
        .synchronized(card_in)
    );

    wire card_enable;
    wire [31:0] card_data;
    id_card_reader #(
        .OVERCARD_PERIOD(2031), // ~4KHz w/ 8x oversampling
        .OVERSAMPLE_RATIO(8)
    ) id_card_reader_mod(
        .clock(vclock), .reset(reset),
        .card_in(card_in),
        .output_enable(card_enable),
        .output_data(card_data)
    );


    localparam COMMAND_WIDTH = 7;
    localparam ADDRESS_WIDTH = 5;
    localparam SIRC_WIDTH = COMMAND_WIDTH + ADDRESS_WIDTH;

    wire raw_ir_input = ~JA[2]; // RPM7140-R produces inverted output
    wire ir_input;
    synchronize #(
        .NUM_SYNC(NUM_SYNC)
    ) ir_input_gen(
        .clock(vclock), .reset(reset),
        .raw(raw_ir_input),
        .synchronized(ir_input)
    );

    wire sirc_enable;
    wire [SIRC_WIDTH - 1:0] sirc_command;
    sirc_receiver #(
        .OVERCARD_PERIOD(4875), // 75us
        .OVERSAMPLE_RATIO(8),
        .COMMAND_WIDTH(7),
        .ADDRESS_WIDTH(ADDRESS_WIDTH)
    ) sirc_receiver_mod(
        .clock(vclock), .reset(reset),
        .sirc_input(ir_input),
        .output_enable(sirc_enable),
        .output_command(sirc_command)
    );
    //assign LED[15:0] = {card_data[3:0], sirc_command};


    localparam DATA_BITS = 8;
    localparam STOP_BITS = 1;

    wire serial_in;
    synchronize #(
        .NUM_SYNC(NUM_SYNC)
    ) serial_in_gen(
        .clock(vclock), .reset(reset),
        .raw(UART_RXD),
        .synchronized(serial_in)
    );

    wire overbaud_enable;
    divider #(
        .PERIOD(846) // ~9600 baud w/ 8x oversampling
    ) overbaud_gen(
        .clock(vclock), .reset(reset),
        .enable(overbaud_enable)
    );

    wire serial_in_overenable;
    wire serial_in_oversample;
    sample serial_in_sample(
        .clock(vclock), .reset(reset),
        .sample_enable(overbaud_enable), .data_in(serial_in),
        .output_enable(serial_in_overenable),
        .data_out(serial_in_oversample)
    );

    wire possible_start_bit;
    edge_detector #(
        .PATTERN(`NEGEDGE)
    ) possible_start_bit_gen(
        .clock(vclock), .reset(reset),
        .data_in(serial_in),
        .data_out(possible_start_bit)
    );

    wire serial_in_busy;
    wire downsample_reset = reset || (possible_start_bit && !serial_in_busy);
    wire serial_in_enable;
    wire serial_in_bit;
    downsample #(
        .DOWNSAMPLE_RATIO(8)
    ) serial_in_downsample(
        .clock(vclock), .reset(downsample_reset),
        .input_enable(serial_in_overenable),
        .data_in(serial_in_oversample),
        .output_enable(serial_in_enable),
        .data_out(serial_in_bit)
    );

    wire serial_enable;
    wire [DATA_BITS - 1:0] serial_in_char;
    rs232_receive_char #(
        .DATA_BITS(DATA_BITS),
        .STOP_BITS(STOP_BITS)
    ) serial_in_receive(
        .clock(vclock), .reset(reset),
        .input_enable(serial_in_enable),
        .input_bit(serial_in_bit),
        .output_enable(serial_enable),
        .output_char(serial_in_char),
        .busy(serial_in_busy)
    );

    wire baud_enable;
    divider #(
        .PERIOD(6770) // ~9600 baud
    ) baud_gen(
        .clock(vclock), .reset(reset),
        .enable(baud_enable)
    );

    // Echo card data via serial out
    wire serial_out;
    wire done;
    wire username_serial_prompt_enable;
    wire [7:0] username_serial_prompt_char;
    wire password_serial_prompt_enable;
    wire [7:0] password_serial_prompt_char;
    wire id_tap_serial_prompt_enable;
    wire [7:0] id_tap_serial_prompt_char;
    wire id_num_serial_prompt_enable;
    wire [7:0] id_num_serial_prompt_char;

    wire [7:0] serial_send_char;
    wire serial_send_enable;
    rs232_send_char #(
        .DATA_BITS(DATA_BITS),
        .STOP_BITS(STOP_BITS)
    ) serial_out_gen(
        .clock(vclock), .reset(reset),
        .start(serial_send_enable),
        .char(serial_send_char),
        .baud_enable(baud_enable),
        .transmit(serial_out),
        .done(done)
    );
    assign UART_TXD = serial_out;
    wire [1:0] debug;
    wire username_serial_start_prompt;
    wire password_serial_start_prompt;
    wire id_tap_serial_start_prompt;
    wire id_num_serial_start_prompt;
    username_serial_prompt username_serial_prompt_mod(
        .clock(vclock),
        .reset(reset),
        .done(done),
        .start(username_serial_start_prompt),
        .serial_send_enable(username_serial_prompt_enable),
        .send_char(username_serial_prompt_char),
        .debug(2'b00)
    );
    //assign LED[1:0] = debug;

    password_serial_prompt password_serial_prompt_mod(
        .clock(vclock),
        .reset(reset),
        .done(done),
        .start(password_serial_start_prompt),
        .serial_send_enable(password_serial_prompt_enable),
        .send_char(password_serial_prompt_char)
    );

    id_tap_serial_prompt id_tap_serial_prompt_mod(
        .clock(vclock),
        .reset(reset),
        .done(done),
        .start(id_tap_serial_start_prompt),
        .serial_send_enable(id_tap_serial_prompt_enable),
        .send_char(id_tap_serial_prompt_char)
    );

    id_num_serial_prompt id_num_serial_prompt_mod(
        .clock(vclock),
        .reset(reset),
        .done(done),
        .start(id_num_serial_start_prompt),
        .serial_send_enable(id_num_serial_prompt_enable),
        .send_char(id_num_serial_prompt_char)
    );
    wire bel_enable;
    wire password_enable;
    wire id_tap_enable;
    serial_output_sel serial_output_sel_mod(
        .clock(vclock),
        .serial_echo_enable(serial_enable),
        .serial_echo_char(serial_in_char),
        .username_serial_prompt_enable(username_serial_prompt_enable),
        .username_serial_prompt_char(username_serial_prompt_char),
        .password_serial_prompt_enable(password_serial_prompt_enable),
        .password_serial_prompt_char(password_serial_prompt_char),
        .password_enable(password_enable),
        .id_tap_serial_prompt_enable(id_tap_serial_prompt_enable),
        .id_tap_serial_prompt_char(id_tap_serial_prompt_char),
        .id_tap_enable(id_tap_enable),
        .id_num_serial_prompt_enable(id_num_serial_prompt_enable),
        .id_num_serial_prompt_char(id_num_serial_prompt_char),
        .id_number_enable(id_number_enable),
        .bel_enable(bel_enable),
        .serial_enable(serial_send_enable),
        .serial_send_char(serial_send_char)
    );


    wire [10:0] hcount;
    wire [9:0] vcount;
    wire hsync, vsync, blank;
    xvga xvga_gen(
        .vclock(vclock), .reset(reset),
        .hcount(hcount), .vcount(vcount),
        .hsync(hsync), .vsync(vsync)
    );

    localparam NUM_CHARS = 128;
    localparam CHAR_SIZE = 32;

    // Section 0: Address Calculation
    wire [`CLOG2(NUM_CHARS) - 1:0] char;
    wire [`CLOG2(CHAR_SIZE) - 1:0] char_row;
    wire [`CLOG2(CHAR_SIZE) - 1:0] char_col;
    wire hsync_address_calc, vsync_address_calc;

    wire [223:0] joke_data_1;
    wire [223:0] joke_data_2;
    wire [223:0] joke_data_3;
    wire [63:0] username_input;
    wire [63:0] password_input;
    wire [63:0] dummy_password_input;
    wire [71:0] id_number_input;
    wire mit_login_enable;
    wire username_enable;
    wire username_input_enable;
    wire id_tapped_enable;
    wire id_number_enable;
    wire authorized_enable;
    wire unauthorized_enable;
    wire sirc_write_enable;
    wire [3:0] sirc_write;
    wire new_sirc_enable;
    wire [3:0] new_sirc_command;

    main_fsm #(
        .CLOCK_FREQUENCY(65_000_000),
        .NUM_CHARS(8),
        .ID_NUM_CHARS(9)
    ) main_fsm_mod(
        .clock(vclock), .reset(reset),
        .serial_enable(serial_enable),
        .serial_char_received(serial_in_char),
        .sirc_enable(sirc_write_enable),
        .sirc_command(sirc_write),
        .card_data(card_data),
        .username_input(username_input),
        .password_input(password_input),
        .dummy_password_input(dummy_password_input),
        .id_number_input(id_number_input),
        .mit_login_enable(mit_login_enable),
        .username_enable(username_enable),
        .username_input_enable(username_input_enable),
        .password_enable(password_enable),
        .id_tap_enable(id_tap_enable),
        .id_tapped_enable(id_tapped_enable),
        .id_number_enable(id_number_enable),
        .authorized_enable(authorized_enable),
        .unauthorized_enable(unauthorized_enable),
        .username_serial_start_prompt(username_serial_start_prompt),
        .password_serial_start_prompt(password_serial_start_prompt),
        .id_tap_serial_start_prompt(id_tap_serial_start_prompt),
        .id_num_serial_start_prompt(id_num_serial_start_prompt),
        .bel_enable(bel_enable)
    );


    wire [1:0]debug_1;
    sirc_deduplicator_2 sirc_deduplicator_2_mod(
        .clock(vclock),.reset(reset),
        .sirc_enable(new_sirc_enable),
        .new_sirc_command(new_sirc_command),
        .sirc_write_enable(sirc_write_enable),
        .sirc_write(sirc_write),
        .debug(debug_1)
    );
    assign LED[1:0] = debug_1;
    sirc_num_conversion sirc_num_conversion_mod(
       .clock(vclock),.reset(reset),
       .sirc_enable(sirc_enable),
       .sirc_command(sirc_command),
       .new_sirc_enable(new_sirc_enable),
       .new_sirc_command(new_sirc_command)
    );

    address_calc #(
        .NUM_CHARS(NUM_CHARS),
        .CHAR_SIZE(CHAR_SIZE)
    ) address_calc_mod(
        .vclock(vclock),
        .hcount(hcount),.vcount(vcount),
        .hsync(hsync),.vsync(vsync),
        .joke_data_1(joke_data_1),
        .joke_data_2(joke_data_2),
        .joke_data_3(joke_data_3),
        .username_input(username_input),
        .password_input(dummy_password_input),
        .id_number_input(id_number_input),
        .mit_login_enable(mit_login_enable),
        .username_enable(username_enable),
        .username_input_enable(username_input_enable),
        .password_enable(password_enable),
        .id_tap_enable(id_tap_enable),
        .id_tapped_enable(id_tapped_enable),
        .id_number_enable(id_number_enable),
        .authorized_enable(authorized_enable),
        .unauthorized_enable(unauthorized_enable),
        .char(char), .char_row(char_row), .char_col(char_col),
        .hsync_out(hsync_address_calc), .vsync_out(vsync_address_calc)
    );

    joke_database joke_database_mod(
        .clock(vclock),
        .read_enable(authorized_enable),
        .joke_data_1(joke_data_1),
        .joke_data_2(joke_data_2),
        .joke_data_3(joke_data_3)
    );

    wire [3:0] r_val;
    wire [3:0] g_val;
    wire [3:0] b_val;
    background_sel background_sel_mod(
        .clock(vclock),
        .reset(reset),
        .char_pixel(char_pixel),
        .authorized_enable(authorized_enable),
        .unauthorized_enable(unauthorized_enable),
        .r_val(r_val),
        .g_val(g_val),
        .b_val(b_val)
   );

    // Section 1: Background and Spritemap ROM Lookup
    wire char_pixel;
    char_spritemap #(
        .ROM_FILE("./res/rom/ascii.rom"),
        .NUM_CHARS(128),
        .CHAR_SIZE(32),
        .PIXEL_WIDTH(1)
    ) ascii_spritemap(
        .clock(vclock),
        .char(char),
        .row(char_row),
        .column(char_col),
        .pixel(char_pixel)
    );

    // Update hsync and vsync for BROM lookup stage(s)
    reg hsync_buf = 1'b0;
    reg vsync_buf = 1'b0;
    reg hsync_buf_2 = 1'b0;
    reg vsync_buf_2 = 1'b0;
    always @(posedge vclock) begin
        hsync_buf <= hsync_address_calc;
        vsync_buf <= vsync_address_calc;
        hsync_buf_2 <= hsync_buf;
        vsync_buf_2 <= vsync_buf;
    end

    // The output of the last stage is connected directly to VGA.
    assign VGA_R = r_val;
    assign VGA_G = g_val;
    assign VGA_B = b_val;
    assign VGA_HS = hsync_buf_2;
    assign VGA_VS = vsync_buf_2;

    hex_display #(
        .REFRESH_PERIOD(1083333) // ~60Hz @ 65MHz clock
    ) hex_display_mod(
        .clock(vclock), .reset(reset),
        .data(card_data),
        .display(SEG),
        .strobe(AN)
    );

endmodule

`endif // TOP_V
