`ifndef DEBOUNCE_V
`define DEBOUNCE_V

`include "./src/hdl/prologue.v"


// Debounce module for noisy inputs
// (e.g. switches or buttons).
// Fully parameterizable DELAY as an improvement to original 6.111 version.
module debounce #(
    // .01 sec with a 100Mhz clock
    parameter DELAY = 1000000
) (
    input wire clock, reset,
    input wire noisy,
    output reg clean
);

    localparam DELAY_WIDTH = `CLOG2(DELAY + 1);
    reg [DELAY_WIDTH - 1:0] count = {DELAY_WIDTH{1'b0}};
    reg new = 1'b0;

    always @(posedge clock) begin
        if (reset) begin
            count <= {DELAY_WIDTH{1'b0}};
            new <= noisy;
            clean <= noisy;
        end else begin
            if (noisy != new) begin
                new <= noisy;
                count <= {DELAY_WIDTH{1'b0}};
            end else if (count < DELAY) begin
                count <= count + 1;
            end else begin
                clean <= new;
            end
        end
    end

endmodule

`endif // DEBOUNCE_V
