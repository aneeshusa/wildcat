`ifndef DIVIDER_V
`define DIVIDER_V

`include "./src/hdl/prologue.v"


module divider #(
    parameter PERIOD = 100_000_000 // in number of cycles of `clock`
) (
    input wire clock, reset,
    output reg enable = 1'b0
);

    localparam COUNT_WIDTH = `CLOG2(PERIOD);
    reg [COUNT_WIDTH - 1:0] count = {COUNT_WIDTH{1'b0}};


    `define divider__reset() begin \
        count <= {COUNT_WIDTH{1'b0}}; \
        enable <= 1'b0; \
    end


    always @(posedge clock) begin
        if (reset) begin
            `divider__reset
        end else begin
            if (count < PERIOD - 1) begin
                count <= count + 1;
                enable <= 1'b0;
            end else begin
                count <= {COUNT_WIDTH{1'b0}};
                enable <= 1'b1;
            end
        end
    end

endmodule

`endif // DIVIDER_V
