`ifndef CHAR_SPRITEMAP_V
`define CHAR_SPRITEMAP_V

`include "./src/hdl/prologue.v"


// Wrapper for the (ASCII) character spritemap BROM
// which automatically indexes into the BROM correctly
module char_spritemap #(
    parameter ROM_FILE = "./res/rom/ascii.rom",
    parameter NUM_CHARS = 128, // Only need basic half of ASCII
    parameter CHAR_SIZE = 32, // Characters are square. Must be a power of 2
    parameter PIXEL_WIDTH = 1
) (
    input wire clock,
    input wire [`CLOG2(NUM_CHARS) - 1:0] char, // ASCII code for the character
    input wire [`CLOG2(CHAR_SIZE) - 1:0] row, // Row in the char sprite
    input wire [`CLOG2(CHAR_SIZE) - 1:0] column, // Column in the char sprite
    output reg [PIXEL_WIDTH - 1:0] pixel
);

    localparam NUM_ADDRESSES = NUM_CHARS * CHAR_SIZE * CHAR_SIZE;

    (* rom_style = "block" *)
    reg [PIXEL_WIDTH - 1:0] pixels[0:NUM_ADDRESSES - 1];


    wire [`CLOG2(NUM_CHARS * CHAR_SIZE * CHAR_SIZE) - 1:0] address = {
        char, row, column
    };


    initial begin
        $readmemb(ROM_FILE, pixels, 0, NUM_ADDRESSES - 1);
    end


    always @(posedge clock) begin
        pixel <= pixels[address];
    end

endmodule

`endif // CHAR_SPRITEMAP_V
