`ifndef SQUARE_WAVE_V
`define SQUARE_WAVE_V

`include "./src/hdl/prologue.v"


// Generate a 50% duty cycle square wave.
// Main use case is to generate the 125Khz carrier frequency
// used for the RFID reading circuit from the 100Mhz system clock.
module square_wave #(
    // How many cycles of the input `clock`
    // should correspond to one period of the `square_wave` output.
    // Should be an even number.
    // Default creates a 125Khz output with a 100Mhz input.
    parameter PERIOD = 800
) (
    input wire clock, reset,
    output reg square_wave = 1'b0
);

    localparam COUNT_MAX = PERIOD / 2;
    localparam COUNT_WIDTH = `CLOG2(COUNT_MAX);
    reg [COUNT_WIDTH - 1:0] count = {COUNT_WIDTH{1'b0}};


    `define square_wave__reset() begin \
        square_wave <= 1'b0; \
        count <= {COUNT_WIDTH{1'b0}}; \
    end


    always @(posedge clock) begin
        if (reset) begin
            `square_wave__reset
        end else begin
            if (count < COUNT_MAX - 1) begin
                count <= count + 1;
            end else begin
                count <= {COUNT_WIDTH{1'b0}};
                square_wave <= ~square_wave;
            end
        end
    end

endmodule

`endif // SQUARE_WAVE_V
