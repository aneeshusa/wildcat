`ifndef DECODE_SIRC_COMMAND_V
`define DECODE_SIRC_COMMAND_V

`include "./src/hdl/prologue.v"


// Takes an incoming SIRC stream on `input_bit`,
// with availability signaled on `input_enable`.
// Decodes `SIRC_WIDTH` wide SIRC commands,
// with output on `output_command`
// with availability signaled on `output_enable`.
module decode_sirc_command #(
    // Official variants of SIRC use 12, 15, or 20
    // Does not handle bitwidths smaller than 2
    parameter SIRC_WIDTH = 12,
    // How many base durations (600us) input_bit will be asserted for
    // to signal START
    parameter START_DURATION = 4
) (
    input wire clock, reset,
    input wire input_enable, input_bit,
    // Pulses high for one cycle when a new command is available
    // This will happen one cycle after the last data bit is
    // available on `input_bit` (when `input_enable` is asserted)
    output reg output_enable = 1'b0,
    // output_command[0] is the first bit received,
    // output_command[SIRC_WIDTH - 1] is last bit received
    output reg [SIRC_WIDTH - 1:0] output_command = {SIRC_WIDTH{1'b0}}
);

    // Buffer the in-progress command as it is built
    // One bit less wide since no need to buffer last bit,
    // as it can be output directly
    reg [SIRC_WIDTH - 2:0] command_buf = {(SIRC_WIDTH - 1){1'b0}};

    // No outputs that depend directly (combinationally) on state bits,
    // so no glitches possible - can use a simple encoding
    localparam STATE_WAIT_FOR_START = 3'b000;
    localparam STATE_FINALIZE_START = 3'b001;
    localparam STATE_START_DATA_BIT =  3'b010;
    localparam STATE_READ_DATA_ZERO =  3'b011;
    localparam STATE_READ_DATA_ONE =  3'b100;
    localparam DEFAULT_STATE = STATE_WAIT_FOR_START;
    reg [2:0] state = DEFAULT_STATE;

    // Counter of how many start bits we have seen so far
    localparam START_BITS_WIDTH = `CLOG2(START_DURATION - 1);
    reg [START_BITS_WIDTH - 1:0] start_bits = {START_BITS_WIDTH{1'b0}};

    // Counter of how many data bits we have seen so far
    // (Does not include start bits)
    localparam DATA_BITS_WIDTH = `CLOG2(SIRC_WIDTH - 1);
    reg [DATA_BITS_WIDTH - 1:0] data_bits = {DATA_BITS_WIDTH{1'b0}};


    // Restart the main FSM, but leave outputs untouched
    `define decode_sirc_command__restart() begin \
        command_buf <= {(SIRC_WIDTH - 1){1'b0}}; \
        state <= DEFAULT_STATE; \
        start_bits <= {START_BITS_WIDTH{1'b0}}; \
        data_bits <= {DATA_BITS_WIDTH{1'b0}}; \
    end


    // Reset all external outputs and restart the FSM
    `define decode_sirc_command__reset() begin \
        `decode_sirc_command__restart \
        output_enable <= 1'b0; \
        output_command <= {SIRC_WIDTH{1'b0}}; \
    end


    always @(posedge clock) begin
        if (reset == 1'b1) begin
            `decode_sirc_command__reset
        end else begin
            if (input_enable == 1'b0) begin
                output_enable <= 1'b0;
            end else if (input_enable == 1'b1) begin
                case (state)
                    // Look for START_DURATION ones
                    STATE_WAIT_FOR_START: begin
                        output_enable <= 1'b0;
                        if (input_bit == 1'b0) begin
                           `decode_sirc_command__restart
                        end else if (input_bit == 1'b1) begin
                           if (start_bits == START_DURATION - 1) begin
                               state <= STATE_FINALIZE_START;
                           end else begin
                               start_bits <= start_bits + 1;
                           end
                        end else begin
                            `decode_sirc_command__reset
                        end
                    end
                    // Ensure we get a zero
                    STATE_FINALIZE_START: begin
                        output_enable <= 1'b0;
                        if (input_bit == 1'b1) begin
                            `decode_sirc_command__restart
                        end else if (input_bit == 1'b0) begin
                            data_bits <= {DATA_BITS_WIDTH{1'b0}};
                            state <= STATE_START_DATA_BIT;
                        end else begin
                            `decode_sirc_command__reset
                        end
                    end
                    // Ensure we get a one
                    STATE_START_DATA_BIT: begin
                        output_enable <= 1'b0;
                        if (input_bit == 1'b0) begin
                            `decode_sirc_command__restart
                        end else if (input_bit == 1'b1) begin
                            state <= STATE_READ_DATA_ZERO;
                        end else begin
                            `decode_sirc_command__reset
                        end
                    end
		    // A one means keep waiting,
                    // a zero means an output zero
                    STATE_READ_DATA_ZERO: begin
                        if (input_bit == 1'b1) begin
                            output_enable <= 1'b0;
                            state <= STATE_READ_DATA_ONE;
                        end else if (input_bit == 1'b0) begin
                            if (data_bits == SIRC_WIDTH - 1) begin
                                output_enable <= 1'b1;
                                output_command <= {
                                    1'b0, command_buf[SIRC_WIDTH - 2:0]
                                };
                                `decode_sirc_command__restart
                            end else begin
                                output_enable <= 1'b0;
                                command_buf[data_bits] <= 1'b0;
                                data_bits <= data_bits + 1;
                                state <= STATE_START_DATA_BIT;
                            end
                        end else begin
                            `decode_sirc_command__reset
                        end
                    end
                    // Ensure we get a zero
                    STATE_READ_DATA_ONE: begin
                        if (input_bit == 1'b1) begin
                            `decode_sirc_command__restart
                        end else if (input_bit == 1'b0) begin
                            if (data_bits == SIRC_WIDTH - 1) begin
                                output_enable <= 1'b1;
                                output_command <= {
                                    1'b1, command_buf[SIRC_WIDTH - 2:0]
                                };
                                `decode_sirc_command__restart
                            end else begin
                                output_enable <= 1'b0;
                                command_buf[data_bits] <= 1'b1;
                                data_bits <= data_bits + 1;
                                state <= STATE_START_DATA_BIT;
                            end
                        end else begin
                            `decode_sirc_command__reset
                        end
                    end
                    default: begin
                        `decode_sirc_command__reset
                    end
                endcase
            end else begin
                `decode_sirc_command__reset
            end
        end
    end

endmodule

`endif // DECODE_SIRC_COMMAND_V
