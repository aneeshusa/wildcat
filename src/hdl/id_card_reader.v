`ifndef ID_CARD_READER
`define ID_CARD_READER

`include "./src/hdl/prologue.v"

`include "./src/hdl/descramble_flexsecure.v"
`include "./src/hdl/divider.v"
`include "./src/hdl/downsample.v"
`include "./src/hdl/psk_decode.v"
`include "./src/hdl/sample.v"


module id_card_reader #(
    parameter OVERCARD_PERIOD = 1000,
    parameter OVERSAMPLE_RATIO = 10
) (
    input wire clock, reset,
    input wire card_in,
    output wire output_enable,
    output wire [31:0] output_data
);

    wire overcard_enable;
    divider #(
        .PERIOD(OVERCARD_PERIOD) // ~4KHz w/ 8x oversampling
    ) overcard_gen(
        .clock(clock), .reset(reset),
        .enable(overcard_enable)
    );

    wire card_in_overenable, card_in_oversample;
    sample card_in_sample(
        .clock(clock), .reset(reset),
        .sample_enable(overcard_enable),
        .data_in(card_in),
        .output_enable(card_in_overenable),
        .data_out(card_in_oversample)
    );

    wire card_in_enable, card_in_bit;
    downsample #(
        .DOWNSAMPLE_RATIO(OVERSAMPLE_RATIO)
    ) card_in_downsample(
        .clock(clock), .reset(reset),
        .input_enable(card_in_overenable),
        .data_in(card_in_oversample),
        .output_enable(card_in_enable),
        .data_out(card_in_bit)
    );

    reg last_card_bit = 1'b0;
    wire [1:0] psk_data = {last_card_bit, card_in_bit};
    wire prox_enable, prox_bit;
    psk_decode card_psk_decode(
        .clock(clock), .reset(reset),
        .input_enable(card_in_enable),
        .input_data(psk_data),
        .output_enable(prox_enable),
        .output_data(prox_bit)
    );

    reg [30:0] old_prox_bits = {31{1'b0}};
    wire [31:0] prox_data = {old_prox_bits, prox_bit};
    descramble_flexsecure #(
        .KEY(32'b1100_1010_1000_0011_1000_1001_0111_1110)
    ) defeat_flexsecure_protection(
        .clock(clock), .reset(reset),
        .prox_enable(prox_enable),
        .prox_data(prox_data),
        .out_enable(output_enable),
        .out_data(output_data)
    );


    `define id_card__reset() begin \
        last_card_bit <= 1'b0; \
        old_prox_bits <= {31{1'b0}}; \
    end


    always @(posedge clock) begin
        if (reset) begin
            `id_card__reset
        end else begin
            if (card_in_enable) begin
                last_card_bit <= psk_data[0];
            end
            if (prox_enable) begin
                old_prox_bits <= prox_data[30:0];
            end
        end
    end

endmodule

`endif // ID_CARD_READER
