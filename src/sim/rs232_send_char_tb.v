`include "./src/hdl/prologue.v"

`include "./src/hdl/divider.v"
`include "./src/hdl/rs232_send_char.v"


module rs232_send_char_tb #(
    parameter BAUD = 8, // Use a small value for testing
    parameter STOP_BITS = 1
);

    // Inputs
    reg clock, reset;
    reg start;
    reg [7:0] char;

    // Outputs
    wire serial_out;
    wire done;

    wire baud_enable;
    divider #(
        .PERIOD(BAUD) // Use a small baud rate for testing
    ) baud_gen(
        .clock(clock), .reset(reset),
        .enable(baud_enable)
    );

    rs232_send_char #(
        .DATA_BITS(8),
        .STOP_BITS(STOP_BITS)
    ) uut(
        .clock(clock), .reset(reset),
        .start(start),
        .char(char),
        .baud_enable(baud_enable),
        .transmit(serial_out),
        .done(done)
    );


    `define rs232_send_char__assert_rs232_bit(value) begin \
        #(BAUD * `CLK); \
        `assert(serial_out, value) \
        `assert(done, 1'b0) \
    end


    always #(`CLK/2) clock = !clock;
    initial begin
        clock = 1; // Start on a posedge clock
        reset = 0;

        // Wait for global reset to finish
        #100;

        // Offset into middle of clock cycle (between posedges)
        #(`CLK/2);

        $display("BEGINNING TEST OF %m");

        `start_test("Send one character")
        `pulse(reset, 1'b1) // Ensure divider is reset
        char = 8'b0100_1010; // ASCII J
        `pulse(start, 1'b1)

        // Wait for first baud_enable after `start`,
        // which is when the FSM can start sending data
        #((BAUD - 1) * `CLK); // `reset` is one cycle before pulse
        `assert(baud_enable, 1'b1)
        #((BAUD/2) * `CLK); // Offset into middle of baud period for sampling

        `assert(serial_out, 1'b0) // Start bit
        // 8 data bits, LSB to MSB
        `rs232_send_char__assert_rs232_bit(1'b0)
        `rs232_send_char__assert_rs232_bit(1'b1)
        `rs232_send_char__assert_rs232_bit(1'b0)
        `rs232_send_char__assert_rs232_bit(1'b1)
        `rs232_send_char__assert_rs232_bit(1'b0)
        `rs232_send_char__assert_rs232_bit(1'b0)
        `rs232_send_char__assert_rs232_bit(1'b1)
        `rs232_send_char__assert_rs232_bit(1'b0)
        repeat (STOP_BITS - 1) begin
            `rs232_send_char__assert_rs232_bit(1'b1) // Stop bits
        end
        // Done should be high the same cycle we send the last stop bit
        // Only wait half a baud cycle to cancel offset,
        // plus one cycle since `done` is pulsed one cycle after `baud_enable`
        #((BAUD - (BAUD/2) + 1) * `CLK);
        `assert(serial_out, 1'b1)
        `assert(done, 1'b1)
        // Done should only be asserted for one cycle,
        // although serial_out is buffered
        #(`CLK);
        `assert(serial_out, 1'b1)
        `assert(done, 1'b0)

        $display("ENDING TEST OF %m");
        $stop();
    end

endmodule
