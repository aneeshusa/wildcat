`include "./src/hdl/prologue.v"

`include "./src/hdl/divider.v"


module divider_tb;

    // Inputs
    reg clock, reset;

    // Outputs
    wire enable;

    localparam PERIOD = 6;

    divider #(
        .PERIOD(6) // Use a small period for ease of testing
    ) uut(
        .clock(clock), .reset(reset),
        .enable(enable)
    );


    always #(`CLK/2) clock = !clock;
    initial begin
        clock = 1; // Start on a posedge clock
        reset = 0;

        // Wait for global reset to finish
        #100;

        #(`CLK/2); // Offset into middle of cycle (between posedges)

        $display("BEGINNING TEST OF %m");

        `start_test("2 cycles of divder after semi-random reset")
        `pulse(reset, 1'b1)

        repeat (PERIOD - 1) begin
            #(`CLK);
            `assert(enable, 1'b0)
        end

        #(`CLK);
        // Now 10 cycles after reset
        `assert(enable, 1'b1)

        repeat (PERIOD - 1) begin
          #(`CLK);
          `assert(enable, 1'b0)
        end

        #(`CLK);
        // Now 20 cycles after reset
        `assert(enable, 1'b1)

        #(`CLK);
        // Now 21 cycles after reset
        `assert(enable, 1'b0)

        $display("ENDING TEST OF %m");
        $stop();
    end

endmodule
