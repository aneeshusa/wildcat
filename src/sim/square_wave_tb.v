`include "./src/hdl/prologue.v"

`include "./src/hdl/square_wave.v"


module square_wave_tb;

    // Inputs
    reg clock, reset;

    // Outputs
    wire square_wave;

    localparam PERIOD = 8; // Use a small value for testing

    square_wave #(
        .PERIOD(PERIOD)
    ) uut(
        .clock(clock), .reset(reset),
        .square_wave(square_wave)
    );


    // Tests timing for one period, starting one cycle after start
    // 4 cycles low, then 4 cycles high
    `define square_wave_tb__test_one_period() begin \
        repeat ((PERIOD/2) - 1) begin \
            #(`CLK); \
            `assert(square_wave, 1'b0) \
        end \
        repeat (PERIOD/2) begin \
            #(`CLK); \
            `assert(square_wave, 1'b1) \
        end \
        #(`CLK); \
        `assert(square_wave, 1'b0) \
    end


    always #(`CLK/2) clock = !clock;
    initial begin
        clock = 1; // Start on a posedge clock
        reset = 0;

        // Wait for global reset to finish
        #100;

        // Offset into middle of clock cycle (between posedges)
        #(`CLK/2);

        $display("BEGINNING TEST OF %m");

        `start_test("2 simple cycles of square wave")
        `pulse(reset, 1'b1)

        `square_wave_tb__test_one_period
        `square_wave_tb__test_one_period

        `start_test("Reset works in middle of period")
        #(5 * `CLK); // Get into middle of square wave
        `pulse(reset, 1'b1)
        `square_wave_tb__test_one_period

        $display("ENDING TEST OF %m");
        $stop();
    end

endmodule
