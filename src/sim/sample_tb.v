`include "./src/hdl/prologue.v"

`include "./src/hdl/sample.v"


module sample_tb;

    // Inputs
    reg clock, reset;
    reg sample_enable, data_in;

    // Outputs
    wire output_enable, data_out;

    sample uut(
        .clock(clock), .reset(reset),
        .sample_enable(sample_enable), .data_in(data_in),
        .output_enable(output_enable), .data_out(data_out)
    );


    always #(`CLK/2) clock = !clock;
    initial begin
        clock = 1; // Start on a posedge clock
        reset = 0;

        // Wait for global reset to finish
        #100;

        // Offset into middle of clock cycle (between posedges)
        #(`CLK/2);

        $display("BEGINNING TEST OF %m");

        `start_test("Take a simple sample")
        // Provide a one bit sample
        `assert(output_enable, 1'b0)
        data_in = 1'b1;
        `pulse(sample_enable, 1'b1)

        // Check for a one cycle wide output enable pulse,
        // as well as output buffering
        `assert(output_enable, 1'b1)
        `assert(data_out, 1'b1)
        #(`CLK);
        `assert(output_enable, 1'b0)
        `assert(data_out, 1'b1)

        `start_test("Check that resetting works")
        `pulse(reset, 1'b1)
        `assert(output_enable, 1'b0)
        `assert(data_out, 1'b0)

        $display("ENDING TEST OF %m");
        $stop();
    end

endmodule
