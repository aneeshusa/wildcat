`include "./src/hdl/prologue.v"

`include "./src/hdl/downsample.v"


module downsample_tb #(
    DOWNSAMPLE_RATIO = 4
);

    // Inputs
    reg clock, reset;
    reg data_in;

    // Outputs
    wire output_enable, data_out;

    localparam PERIOD = 3; // Use a small period for ease of testing

    wire input_enable;
    divider #(
        .PERIOD(PERIOD)
    ) divider_mod(
        .clock(clock), .reset(reset),
        .enable(input_enable)
    );

    downsample #(
        .DOWNSAMPLE_RATIO(DOWNSAMPLE_RATIO)
    ) uut(
        .clock(clock), .reset(reset),
        .input_enable(input_enable), .data_in(data_in),
        .output_enable(output_enable), .data_out(data_out)
    );

    // Latency from start until value is available
    localparam LATENCY = DOWNSAMPLE_RATIO * PERIOD;


    `define downsample_tb__check_output(value) begin \
        `assert(output_enable, 1'b0) \
        #(`CLK) \
        // Output bit is now available \
        `assert(output_enable, 1'b1) \
        `assert(data_out, value) \
        #(`CLK) \
        // `output_enable` should de-assert after one cycle, \
        // although `data_out` is still buffered \
        `assert(output_enable, 1'b0) \
        `assert(data_out, value) \
    end


    always #(`CLK/2) clock = !clock;
    initial begin
        clock = 1; // Start on a posedge clock
        reset = 0;

        // Wait for global reset to finish
        #100;

        // Offset into middle of clock cycle (between posedges)
        #(`CLK/2);

        $display("BEGINNING TEST OF %m");

        // FIRST SECTION: synchronous resets
        // divider is reset at same time as downsampler

        `start_test("All inputs are zero")
        data_in = 1'b0;
        `pulse(reset, 1'b1)

        repeat (PERIOD - 1) begin
            #(`CLK);
            // No data yet (after reset, before first divider)
            `assert(output_enable, 1'b0)
        end

        #(`CLK);
        // `PERIOD` cycles after reset,
        // first sample taken but no data yet
        `assert(input_enable, 1'b1)
        `assert(output_enable, 1'b0)

        #((LATENCY - PERIOD - 1) * `CLK);
        // 1 cycles before taking last sample, still no data
        `assert(output_enable, 1'b0)

        #(`CLK);
        // `DOWNSAMPLE_RATIO` * `PERIOD` cycles after reset, last sample taken
        `downsample_tb__check_output(1'b0)


        `start_test("All inputs are one")
        data_in = 1'b1;
        `pulse(reset, 1'b1)

        #(LATENCY * `CLK);
        `downsample_tb__check_output(1'b1)


        `start_test("Inputs are mostly ones, with a glitch to zero")
        data_in = 1'b1;
        `pulse(reset, 1'b1)

        #((LATENCY - PERIOD) * `CLK);
        // Have last input bit be a zero instead of a one
        data_in = 1'b0;

        #(PERIOD * `CLK);
        `downsample_tb__check_output(1'b1);


        `start_test("Inputs are evenly mixed zeros and ones")
        data_in = 1'b0; // Start with half zeros
        `pulse(reset, 1'b1)

        #((LATENCY/2) * `CLK);
        data_in = 1'b1; // Switch to half ones

        // Subtract half from whole to handle odd values
        #(((LATENCY) - (LATENCY/2)) *`CLK);
        `downsample_tb__check_output(1'b1);


        $display("ENDING TEST OF %m");
        $stop();
    end

endmodule
