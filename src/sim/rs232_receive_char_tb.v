`include "./src/hdl/prologue.v"

`include "./src/hdl/rs232_receive_char.v"


module rs232_receive_char_tb #(
    parameter STOP_BITS = 1
);

    localparam DATA_BITS = 8;

    // Inputs
    reg clock, reset;
    reg input_enable, input_bit;

    // Outputs
    wire output_enable;
    wire [DATA_BITS - 1:0] output_char;

    rs232_receive_char #(
        .DATA_BITS(DATA_BITS),
        .STOP_BITS(STOP_BITS)
    ) uut(
        .clock(clock), .reset(reset),
        .input_enable(input_enable), .input_bit(input_bit),
        .output_enable(output_enable), .output_char(output_char)
    );

    integer i;
    wire [7:0] ascii_j = 8'b0100_1010;


    `define feed_bit(value) begin \
        `assert(output_enable, 1'b0) \
        input_bit = value; \
        input_enable = 1'b1; \
        #(`CLK); \
        input_enable = 1'b0; \
    end


    always #(`CLK/2) clock = !clock;
    initial begin
        clock = 1; // Start on a posedge clock
        reset = 0;

        // Wait for global reset to finish
        #100;

        // Offset into middle of clock cycle (between posedges)
        #(`CLK/2);

        $display("BEGINNING TEST OF %m");

        `start_test("Read a regular frame with one character")
        `feed_bit(1'b0) // Start bit
        for (i = 0; i < 8; i = i + 1) begin
            `feed_bit(ascii_j[i]);
        end
        repeat (STOP_BITS) begin
            `feed_bit(1'b1)
        end

        // Ensure output is posted on time
        `assert(output_enable, 1'b1)
        `assert(output_char, ascii_j)

        #(`CLK);
        // Ensure output pulse is only one cycle, but char is buffered
        `assert(output_enable, 1'b0)
        `assert(output_char, ascii_j)


        `start_test("Try to read a frame with one too few stop bits")
        `feed_bit(1'b0) // Start bit
        for (i = 0; i < 8; i = i + 1) begin
            `feed_bit(ascii_j[i]);
        end
        // All but the last stop bit
        repeat (STOP_BITS - 1) begin
            `feed_bit(1'b1)
        end
        `feed_bit(1'b0) // Send invalid stop bit for last value
        `assert(output_enable, 1'b0)


        `start_test("Reset on final data bit input")
        `feed_bit(1'b0) // Start bit
        for (i = 0; i < 8; i = i + 1) begin
            `feed_bit(ascii_j[i]);
        end
        // All but the last stop bit
        repeat (STOP_BITS - 1) begin
            `feed_bit(1'b1)
        end
        reset = 1'b1; // Feed a reset with the last stop bit
        `feed_bit(1'b1)
        reset = 1'b0;
        `assert(output_enable, 1'b0)
        `assert(output_char, 8'b0000_0000) // Reset also resets buffered char

        // TODO: other test cases:
        // - Nonzero/constant/arbitrary delays between input bits
        // - Feed too few start bits, abort start sequence, ensure no output
        // - Feed all start bits, abort after n-1 data bits, ensure no output

        $display("ENDING TEST OF %m");
        $stop();
    end

endmodule
