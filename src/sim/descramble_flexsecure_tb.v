`include "./src/hdl/prologue.v"

`include "./src/hdl/descramble_flexsecure.v"


module descramble_flexsecure_tb;

    // Inputs
    reg clock, reset;
    reg prox_enable;
    reg [31:0] prox_data;

    // Outputs
    wire out_enable;
    wire [31:0] out_data;

    localparam MIT_FLEXSECURE_KEY = 32'b1100_1010_1000_0011_1000_1001_0111_1110;

    descramble_flexsecure #(
        .KEY(MIT_FLEXSECURE_KEY)
    ) uut(
        .clock(clock), .reset(reset),
        .prox_enable(prox_enable), .prox_data(prox_data),
        .out_enable(out_enable), .out_data(out_data)
    );


    always #(`CLK/2) clock = !clock;
    initial begin
        clock = 1; // Start on a posedge clock
        reset = 0;

        // Wait for global reset to finish
        #100;

        // Offset into middle of clock cycle (between posedges)
        #(`CLK/2);

        $display("BEGINNING TEST OF %m");


        `start_test("Austin Roach's MIT ID")
        prox_data <= 32'b1001_0010_0010_0110_1000_0001_0010_0101;
        `assert(out_enable, 1'b0)

        `pulse(prox_enable, 1'b1)
        // Only 1 cycle latency

        `assert(out_enable, 1'b1)
        `assert(out_data, 32'b0010_1110_1010_0101_0000_1000_0101_1011)
        #(`CLK); // One cycle enable, buffered output
        `assert(out_enable, 1'b0)
        `assert(out_data, 32'b0010_1110_1010_0101_0000_1000_0101_1011)


        $display("ENDING TEST OF %m");
        $stop();
    end

endmodule
