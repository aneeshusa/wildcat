`include "./src/hdl/prologue.v"

`include "./src/hdl/edge_detector.v"


module edge_detector_tb;

    // Inputs
    reg clock, reset;
    reg data_in;

    // Outputs
    wire data_out;

    edge_detector #(
        .PATTERN(`POSEDGE)
    ) uut(
        .clock(clock), .reset(reset),
        .data_in(data_in),
        .data_out(data_out)
    );


    always #(`CLK/2) clock = !clock;
    initial begin
        clock = 1; // Start on a posedge clock
        reset = 0;

        // Wait for global reset to finish
        #100;

        // Offset into middle of clock cycle (between posedges)
        #(`CLK/2);

        $display("[ INFO ] BEGINNING TEST OF %m");

        `start_test("Continuous zeros produces nothing")
        data_in <= 1'b0;

        #(`CLK);
        repeat (8) begin
            #(`CLK);
            `assert(data_out, 1'b0)
        end

        `start_test("Positive edge produces single cycle pulse")
        data_in <= 1'b0;
        #(`CLK);
        data_in <= 1'b1;
        #(`CLK);

        `assert(data_out, 1'b1)

        `start_test("Continuous ones produces nothing")
        data_in <= 1'b1;

        repeat (8) begin
            #(`CLK);
            `assert(data_out, 1'b0)
        end

        `start_test("Negative edge produces nothing")
        data_in <= 1'b1;
        #(`CLK);
        data_in <= 1'b0;
        #(`CLK);

        `assert(data_out, 1'b0)

        `start_test("Reset overrides positive edge")
        data_in <= 1'b0;
        #(`CLK);
        reset <= 1'b1;
        data_in <= 1'b1;
        #(`CLK);
        reset <= 1'b0;

        `assert(data_out, 1'b0)

        $display("");
        $display("[ INFO ] ENDING TEST OF %m");
        $stop();
    end

endmodule
