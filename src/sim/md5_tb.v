`include "./src/hdl/prologue.v"

`include "./src/hdl/md5.v"


module md5_tb #(
    parameter DATA = "",
    parameter INPUT_WIDTH = 0,
    parameter DIGEST = 128'hd41d8cd98f00b204e9800998ecf8427e
);

    localparam NUM_CHUNKS = 1 + ((INPUT_WIDTH + 1) / 512) + (
        ((INPUT_WIDTH + 1) % 512) > 448 ? 1 : 0
    );

    // Inputs
    reg clock, reset;
    reg start;
    reg [INPUT_WIDTH - 1:0] input_data;

    // Outputs
    wire output_enable;
    wire [127:0] output_data;

    md5 #(
        .INPUT_WIDTH(INPUT_WIDTH)
    ) uut(
        .clock(clock), .reset(reset),
        .start(start), .input_data(input_data),
        .output_enable(output_enable), .output_data(output_data)
    );


    always #(`CLK/2) clock = !clock;
    initial begin
        clock = 1; // Start on a posedge clock
        reset = 0;

        // Wait for global reset to finish
        #100;

        // Offset into middle of clock cycle (between posedges)
        #(`CLK/2);

        $display("BEGINNING TEST OF %m");


        `start_test("MD5SUM of given value")
        start <= 1'b0;
        `pulse(reset, 1'b1)
        input_data <= DATA;
        `pulse(start, 1'b1)

        // One cycle to warm up, one to finalize, and per chunk:
        //   One to warm up, 64 * 2 for the loop, and one to finalize
        // Then subtract one implicit in pulsing the start
        #((1 + NUM_CHUNKS * (1 + 64 + 64 + 1)) * `CLK);
        `assert(output_enable, 1'b1)
        `assert(output_data, DIGEST);
        // `output_enable` should pulse, but `output_data` should be buffered
        #(`CLK);
        `assert(output_enable, 1'b0);
        `assert(output_data, DIGEST);

        `start_test("Do it again")
        `pulse(start, 1'b1)

        // One cycle to warm up, one to finalize, and per chunk:
        //   One to warm up, 64 * 2 for the loop, and one to finalize
        // Then subtract one implicit in pulsing the start
        #((1 + NUM_CHUNKS * (1 + 64 + 64 + 1)) * `CLK);
        `assert(output_enable, 1'b1)
        `assert(output_data, DIGEST);
        // `output_enable` should pulse, but `output_data` should be buffered
        #(`CLK);
        `assert(output_enable, 1'b0);
        `assert(output_data, DIGEST);


        $display("ENDING TEST OF %m");
        $stop();
    end

endmodule
