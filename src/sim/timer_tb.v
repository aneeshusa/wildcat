`include "./src/hdl/prologue.v"

`include "./src/hdl/timer.v"


module timer_tb;

    localparam MAX_TIMER_LENGTH = 8; // Use a non-default for testing
    localparam LENGTH_WIDTH = `CLOG2(MAX_TIMER_LENGTH + 1);

    // Inputs
    reg clock, reset;
    reg start;
    reg [LENGTH_WIDTH - 1:0] timer_length;

    // Outputs
    wire expired;

    localparam CLOCK_FREQUENCY = 4; // Use a small period for ease of testing

    timer #(
        .CLOCK_FREQUENCY(CLOCK_FREQUENCY),
        .MAX_TIMER_LENGTH(MAX_TIMER_LENGTH)
    ) uut(
        .clock(clock), .reset(reset),
        .start(start), .timer_length(timer_length),
        .expired(expired)
    );


    // Tests timing for one expiration of the timer,
    // starting one cycle after start
    `define timer_tb__test_one_expiration(length_cycles) begin \
        if (length_cycles > 0) begin \
            repeat (length_cycles) begin \
                #(`CLK); \
                `assert(expired, 1'b0) \
            end \
            #(`CLK); \
        end \
        `assert(expired, 1'b1) \
        #(`CLK); \
        `assert(expired, 1'b0) \
    end


    always #(`CLK/2) clock = !clock;
    initial begin
        clock = 1; // Start on a posedge clock
        reset = 0;
        start = 0;

        // Wait for global reset to finish
        #100;

        // Offset into middle of clock cycle (between posedges)
        #(`CLK/2);

        $display("BEGINNING TEST OF %m");

        `start_test("Run the timer once")
        timer_length = 4'b0010; // 2 lengths * 4 cycles/length => 8 cycle timer
        `pulse(start, 1'b1)

        `timer_tb__test_one_expiration(8)

        // Timer should not fire again
        // (even at 16th cycle, which would be a second period of 8)
        repeat (7) begin
            #(`CLK);
            `assert(expired, 1'b0)
        end


        `start_test("Run the timer with length 0")
        // 0 lengths * 4 cycles/length => 0 cycle timer
        timer_length = {LENGTH_WIDTH{1'b0}};
        `pulse(start, 1'b1)

        `timer_tb__test_one_expiration(0)


        `start_test("Run the timer with the max count")
        timer_length = MAX_TIMER_LENGTH;
        `pulse(start, 1'b1)

        `timer_tb__test_one_expiration(MAX_TIMER_LENGTH * CLOCK_FREQUENCY)


        `start_test("Restart the timer while it is already running")
        // Still a 12 cycle timer
        `pulse(start, 1'b1)

        repeat (6) begin
            #(`CLK);
            `assert(expired, 1'b0)
        end
        // Six cycles after start, restart the timer
        timer_length = 4'b0101; // 5 lengths => 20 cycle timer
        `pulse(start, 1'b1)

        // Twelve cycles after the first start (5 cycles after the restart),
        // the timer would have fired but shouldn't due to restart,
        // so the timer should not expire until a full 20 cycles after restart
        `timer_tb__test_one_expiration(20)


        `start_test("Start a zero-length timer while it is already running")
        timer_length = 4'b0010; // 2 lengths => 8 cycle timer
        `pulse(start, 1'b1)

        #(2 * `CLK); // Wait until middle of cycle
        timer_length = {LENGTH_WIDTH{1'b0}};
        `pulse(start, 1'b1)

        `timer_tb__test_one_expiration(0)

        // Make sure the original 8-cycle timer was forgotten
        // and doesn't fire
        repeat (10) begin
            #(`CLK);
            `assert(expired, 1'b0)
        end

        $display("ENDING TEST OF %m");
        $stop();
    end

endmodule
