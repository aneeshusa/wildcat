## Timing constraints (need for synthesis and implementation)
## Ordering used is the one recommended in UG903


## Timing Assertions Section

# Primary clocks
create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports {CLK100MHZ}];

# Virtual clocks

# Generated clocks

# Clock Groups

# Input and output delay constraints


## Timing Exceptions Section

# False Paths

# Max Delay / Min Delay

# Multicycle Paths

# Case Analysis

# Disable Timing

