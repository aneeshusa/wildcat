create_ip -name clk_wiz -vendor xilinx.com -library ip -version 5.3 -module_name clockgen

set_property -dict [list \
    CONFIG.PRIMARY_PORT {clock} \
    CONFIG.CLK_OUT1_PORT {vclock} \
    CONFIG.PRIM_SOURCE {No_buffer} \
    CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {65.000} \
    CONFIG.MMCM_DIVCLK_DIVIDE {5} \
    CONFIG.MMCM_CLKFBOUT_MULT_F {50.375} \
    CONFIG.MMCM_CLKOUT0_DIVIDE_F {15.500} \
    CONFIG.CLKOUT1_JITTER {254.866} \
    CONFIG.CLKOUT1_PHASE_ERROR {297.890} \
] [get_ips clockgen]
