# TCL script to build the project from scratch.

## SETUP
# Check pre-requisites
if { [pwd] ne [file dirname [file normalize [info script ]]] } {
    puts stderr "error: Run build.tcl from the directory it is in."
    exit 1
}

# Setup constants and output dir
set BUILD_DIR "./build"
file delete -force -- "${BUILD_DIR}"
file mkdir "${BUILD_DIR}"

# Used internally by Vivado for IP generation
file delete -force -- "./.srcs"

set PROJECT_NAME "wildcat"
set PART "xc7a100tcsg324-3"
set_part "${PART}"

## ROM creation
file mkdir "${BUILD_DIR}/rom"
exec ./util/gen_id_db.py \
    ./res/rom/identity_database.txt \
    >${BUILD_DIR}/rom/identity_database.rom

## SYNTHESIS
# Read in files needed for synthesis and implementation.
# Specify files individually to avoid loading untracked/in progress files.

# Read IP files first because they (mostly) require that `default_nettype
# be unchanged, i.e. be set to wire, and will not work once
# it is set to `none` when reading our verilog.
set IP_DIR "./src/ip"
foreach ip_file [glob -tails -dir "${IP_DIR}" *.tcl] {
    # Create the IP and set the necessary properties
    source "${IP_DIR}/${ip_file}"
    set ip [file rootname "${ip_file}"]
    set_property generate_synth_checkpoint false [get_files "${ip}.xci"]
    generate_target all [get_ips "${ip}"]
}
report_ip_status

# All verilog files should use `include to pull in dependencies.
read_verilog ./src/hdl/top.v

# NOTE: constraint order matters since they are executed serially.
read_xdc {
    ./src/constraints/nexys4ddr_rev_c_timing.xdc
}

# Run synthesis and optimization.
synth_design -top nexys4 -part "${PART}"

opt_design
power_opt_design


## IMPLEMENTATION
# Read in implementation-only files.
read_xdc {
    ./src/constraints/nexys4ddr_rev_c_physical.xdc
}

# Run place and route, and write bitstream
place_design
phys_opt_design

route_design

write_bitstream -force "${BUILD_DIR}/wildcat.bit"


# NOTE: programming must currently be done manually,
# hence the gui is started at the end.
start_gui
