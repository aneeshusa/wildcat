let

  # Use a pinned version of nixpkgs
  # This commit hash can be changed to update the nixpkgs pin
  version = "8ef3eaeb4e531929ec29a880cb4c67f790e5eb70";

  pin = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${version}.tar.gz";
    # TODO: pin sha256 when Nix 1.12 is released
  }) {};

in

  pin
