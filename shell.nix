{ pkgs ? import ./nix/nixpkgs.nix }:

let

  env = pkgs.buildEnv {
    name = "bmp2memb";
    extraOutputsToInstall = [ "bin" "man" ];
    paths = with pkgs; [
      (python35.withPackages (ps: with ps; [ pillow ]))
    ];
  };

in
  pkgs.stdenv.mkDerivation {
    name = "bmp2memb";
    buildInputs = [ env ];
  } // { inherit env; }
