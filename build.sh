#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail


main() {
    vivado -m64 -mode batch -notrace -source ./build.tcl
}


main "$@"
