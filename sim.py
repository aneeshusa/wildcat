#!/usr/bin/env python3

import glob
import itertools
import json
import os
import subprocess
import sys


def usage(arg0: str):
    print("usage: {} [-h|--help|<testbench_name>]".format(arg0))


def sim(testbench: str, *, headless: bool, params) -> int:
    testbench_file = "./src/sim/{}.v".format(testbench)
    if not os.path.isfile(testbench_file):
        msg ="Could not find testbench {} at {}".format(
            testbench, testbench_file
        )
        print(msg, file=sys.stderr)
        return 1

    snapshot = "{}_sim".format(testbench)
    subprocess.check_call(['xvlog', testbench_file])

    subprocess.check_call(
        ['xelab', '-debug', 'typical', '-s', snapshot] + list(itertools.chain(
           *(['-generic_top', '{}={}'.format(k, v)] for k, v in params.items())
        )) + [testbench]
    )

    if headless:
        proc = subprocess.Popen(
            ['xsim', '-R', snapshot],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT
        )
        proc.wait()
        any_failures = False
        for l in proc.stdout.readlines():
            sys.stdout.buffer.write(l)
            any_failures |= b'[ FAIL ]' in l
        sys.stdout.buffer.flush()
        return 1 if any_failures else 0
    else:
        return subprocess.check_call(
            ['xsim', '-gui', '-tclbatch', 'sim.tcl', snapshot]
        )


def get_testbench_variants():
    with open('./src/sim/variants.txt') as variants_file:
        variants = json.load(variants_file)
    for tb in glob.iglob('./src/sim/*.v'):
        tb = tb[10:-2] # Strip off extra path bits
        if tb in variants:
            for variant in variants[tb]:
                yield (tb, variant)
        else:
            yield (tb, {})


def format_tb(tb_info):
    tb, params = tb_info
    extra = ''
    if params:
        formatted_params = ("{}={}".format(k, v) for k, v in params.items())
        extra = ' ({})'.format(", ".join(formatted_params))
    return '{}{}'.format(tb, extra)


def main(argv) -> int:
    if len(argv) == 1:
        testbenches = list(get_testbench_variants())
        failures = []
        for testbench, params in testbenches:
            ret = sim(testbench, headless=True, params=params)
            if ret !=0:
                failures.append((testbench, params))
        ok = len(failures) == 0
        print("\n[ {} ] {}:".format(
            "PASS" if ok else "FAIL",
            "All testbenches passed" if ok else "Some testbenches failed"
        ))
        for tb in (testbenches if ok else failures):
            print("         - {}".format(format_tb(tb)))
        return len(failures)

    if argv[1] == "-h" or argv[1] == "--help":
        usage(argv[0])
        return 0

    headless = False
    params = {}
    for arg in argv[2:]:
        if arg == '--headless':
            headless = True
        else:
            key, _, value = arg.partition('=')
            params[key] = value
    return sim(argv[1], headless=headless, params=params)


if __name__ == '__main__':
    sys.exit(main(sys.argv))
