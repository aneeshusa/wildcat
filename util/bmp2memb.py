#!/usr/bin/env python3

import sys

from PIL import Image


USAGE = """\
usage: {} [--help] <input_image>"""


ONEBIT = {
    (0, 0, 0): 0,
    (255, 255, 255): 1,
}


def main(argv):
    usage = USAGE.format(argv[0])

    if len(argv) <= 1:
        print(usage, file=sys.stderr)
        return 1

    for arg in argv[1:]:
        if arg == '--help':
            print(usage)
            return 0

    with open(argv[1], 'rb') as image_file:
        image = Image.open(image_file)
        pixels = list(map(lambda x: ONEBIT[x], image.getdata()))
        for pixel in pixels:
            print(pixel)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
