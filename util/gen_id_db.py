#!/usr/bin/env python3

import hashlib
import sys


USAGE = """\
usage: {} [ -h | --help ]"""

def str2hex(s):
    return "".join("{:02x}".format(ord(c)) for c in s)


def main(argv):
    usage = USAGE.format(argv[0])

    if len(argv) <= 1:
        print(usage, file=sys.stderr)
        return 1

    for arg in argv[1:]:
        if arg == '-h' or arg == '--help':
            print(usage)
            return 0

    with open(argv[1], 'r') as id_db_info_file:
        for line in id_db_info_file.readlines():
            line = line.rstrip() # Strip newline

            username, password, id_num = line.split(',')
            # Pad username and password to length
            username = "{0: <8}".format(username)
            password = "{0: <8}".format(password)
            assert(len(id_num) == 9)

            m = hashlib.md5()
            m.update(password.encode('utf-8'))
            digest = m.hexdigest()

            print("{}_{}_{}".format(
                str2hex(username),
                digest,
                str2hex(id_num)
            ))

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
